﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity
{
    public enum GameState
    {
        MainMenu, SPLoading, SPPause, InSP, SPGameOver, SPCompleted,

        MPLoading, MPPause, InMP, MPServerStart, MPClientDiscover, MPMenu, MPCompleted, MPGameOver
    }
}
