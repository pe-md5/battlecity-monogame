﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.AI
{
    public class Cell : IPathNode<Object>, IEquatable<Cell>
    {
        public Int32 X { get; set; }
        public Int32 Y { get; set; }
        public Boolean IsWall { get; set; }

        public bool IsWalkable(Object unused)
        {
            return !IsWall;
        }

        public Cell(int x, int y, bool isWall = false)
        {
            X = x;
            Y = y;
            IsWall = isWall;
        }

        public Cell()
        {

        }

        public bool Equals(Cell other)
        {
            if (X == other.X && Y == other.Y)
            {
                return true;
            }
            return false;
        }
    }
}
