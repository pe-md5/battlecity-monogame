﻿using BattleCity.AI;
using BattleCity.Sprite;
using BattleCity.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Utils
{
    public static class Helper
    {

        public static List<Cell> AStarPossibleEndPositions;
        public static List<Cell> PossibleEagleFoundCellPositions;

		private static readonly Random getrandom = new Random();
		private static readonly object syncLock = new object();
		public static int GetRandomNumber(int min, int max)
		{
			lock(syncLock) { // synchronize
				return getrandom .Next(min, max);
			}
		}

        static Helper()
        {
            var temp = Settings.EagleCellPosition;

            AStarPossibleEndPositions = new List<Cell>();

            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y));
            AStarPossibleEndPositions.Add(new Cell(temp.X, temp.Y - 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X, temp.Y + 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y - 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y + 1));

            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y));
            AStarPossibleEndPositions.Add(new Cell(temp.X, temp.Y - 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X, temp.Y + 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y - 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y - 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y + 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y + 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y - 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y + 2));
            
            PossibleEagleFoundCellPositions = new List<Cell>();
            PossibleEagleFoundCellPositions.AddRange(AStarPossibleEndPositions);

            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y));
            AStarPossibleEndPositions.Add(new Cell(temp.X, temp.Y - 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X, temp.Y + 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y - 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y - 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y - 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y - 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y - 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y + 1));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y + 2));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 3, temp.Y + 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 2, temp.Y + 3));
            AStarPossibleEndPositions.Add(new Cell(temp.X - 1, temp.Y + 3));
        }

        public static Vector2 CalculateEnemyPixelPosFromCellPos(Cell cellPosition)
        {
            return new Vector2(cellPosition.Y * Settings.StageElementSize.Y +
                   Settings.StagePosition.Y + Settings.StageBorderSize + 2.5f,
                   cellPosition.X * Settings.StageElementSize.X +
                   Settings.StagePosition.X + Settings.StageBorderSize + 2.5f);
        }

		public static float CalculateEnemyPathFindingTime(float hardnessFactor)
        {
			return Settings.InitialPathFindingTimeOut * (0.75f + hardnessFactor);
        }

		public static float CalculateEnemyRandomTime(float hardnessFactor)
        {
			return Settings.InitialRandomTimeOut * (float)Math.Max(0.001f, 1.0f - hardnessFactor);
        }

        public static Vector2 CalculateTextPositionForGUI(BasicSprite sprite, string text, SpriteFont font)
        {
            var imageSize = sprite.GetSpriteImage().GetCurrentImageSize();
            var textSize = font.MeasureString(text);
            return new Vector2(sprite.Position.X + (imageSize.X - textSize.X) / 2,
                sprite.Position.Y + (imageSize.Y - textSize.Y) / 2);
        }

        public static AnimatedSpriteImage[] GetTankAnimatedSpriteImageArray()
        {
            var temp = new AnimatedSpriteImage[8];
            var textures = Resources.TankTextures;
            for(int i = 0; i < textures.Length; i++)
            {
                if (i < 4)
                {
                    temp[i] = new AnimatedSpriteImage(textures[i], 5, Settings.PlayerAnimationSecPerFrame);
                }
                else
                {
                    temp[i] = new AnimatedSpriteImage(textures[i], 1, float.MaxValue);
                }
            }
            return temp;
        }
    }
}
