﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Utils
{
    public static class StageDescriptions
    {
        static List<string> descriptions = new List<string>();

        public static int NumberOfStages
        {
            get { return descriptions.Count; }
        }

        static StageDescriptions()
		{
            descriptions.Add(
				"E,E,E,E,E,E,E,E,E,E,E,E,E;"+
				"E,BR,E,BR,E,BR,E,BR,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,BR,E,BR,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,BR,SW,BR,E,BR,E,BR,E;"+
				"E,E,E,E,E,E,E,E,E,E,E,E,E;"+
				"SW,E,BR,BR,E,BR,E,BR,E,BR,BR,E,SW;"+
				"E,E,E,E,E,E,E,E,E,E,E,E,E;"+
				"E,E,E,E,E,BR,E,BR,E,E,E,E,E;"+
				"E,BR,E,BR,E,BR,BR,BR,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,BR,E,BR,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,E,E,E,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,BR,BR,BR,E,BR,E,BR,E;"+
				"E,E,P1,E,E,BR,EA,BR,E,E,P2,E,E;"
			);

            descriptions.Add(
				"E,E,E,SW,E,E,E,SW,E,E,E,E,E;" +
                "E,BR,E,SW,E,E,E,BR,E,BR,E,BR,E;" +
                "E,BR,E,E,E,E,BR,BR,E,BR,SW,BR,E;" +
                "E,E,E,BR,E,E,E,E,E,SW,E,E,E;" +
                "BU,E,E,BR,E,E,SW,E,E,BR,BU,BR,SW;" +
                "BU,BU,E,E,E,BR,E,E,SW,E,BU,E,E;" +
                "E,BR,BR,BR,BU,BU,BU,SW,E,E,BU,BR,E;" +
                "E,E,E,SW,BU,BR,E,BR,E,BR,E,BR,E;" +
                "SW,BR,E,SW,E,BR,E,BR,E,E,E,BR,E;" +
                "E,BR,E,BR,E,BR,BR,BR,E,BR,SW,BR,E;" +
                "E,BR,E,BR,E,E,E,E,E,E,E,E,E;" +
                "E,BR,E,E,E,BR,BR,BR,E,BR,E,BR,E;" +
                "E,BR,E,BR,P1,BR,EA,BR,P2,BR,BR,BR,E;"
			);

			descriptions.Add (
				"E,E,E,E,BR,E,E,E,BR,E,E,E,E;"+
				"E,BU,BU,BU,BR,E,E,E,E,E,SW,SW,SW;"+
				"BR,BU,BU,BU,E,E,E,E,E,E,E,E,E;"+
				"BU,BU,BU,BU,E,E,E,BR,E,BR,BR,BR,BR;"+
				"BU,BU,BU,BU,BR,BR,BR,BR,E,BR,E,BR,E;"+
				"E,BU,E,E,E,E,BR,E,E,E,E,BR,E;"+
				"E,E,E,E,E,E,SW,SW,SW,E,E,BU,E;"+
				"E,BR,E,E,BR,E,E,E,E,BU,BU,BU,BU;"+
				"BR,BR,E,BR,BR,BR,E,E,E,BU,BU,BU,BU;"+
				"E,E,E,E,E,E,E,E,E,BU,BU,BU,BU;"+
				"BR,E,E,SW,E,E,E,E,E,BU,BU,BU,E;"+
				"BR,BR,E,SW,E,BR,BR,BR,E,BU,BU,BU,E;"+
				"SW,BR,BR,P1,E,BR,EA,BR,E,BR,P2,E,E;"
			);

			descriptions.Add (
				"E,E,E,E,BR,BR,E,E,E,E,E,E,E;"+
				"SW,E,BR,E,E,E,E,E,SW,SW,SW,E,E;"+
				"BR,E,BR,BR,BR,E,E,E,E,E,E,E,E;"+
				"E,E,E,E,BR,E,E,E,E,WA,WA,E,WA;"+
				"E,E,E,E,E,E,E,E,E,WA,E,E,E;"+
				"BR,BR,E,E,WA,WA,E,WA,WA,WA,E,BR,BR;"+
				"E,E,E,E,WA,BR,E,BR,E,E,E,E,E;"+
				"WA,WA,WA,E,WA,E,E,E,E,E,SW,E,E;"+
				"E,E,E,E,E,E,SW,E,BR,E,SW,BR,BR;"+
				"E,E,E,E,BR,BR,BR,BR,BR,E,E,E,E;"+
				"E,E,E,E,E,E,E,E,E,BR,E,E,E;"+
				"BR,BR,BR,E,E,BR,BR,BR,E,E,BR,E,E;"+
				"BR,E,E,P1,E,BR,EA,BR,E,P2,E,E,E;"
			);

			descriptions.Add (
				"E,E,E,E,E,BR,E,BR,E,BU,E,E,E;"+
				"E,BR,SW,E,E,E,E,E,E,BU,E,BR,BU;"+
				"E,BR,E,BR,E,E,BR,E,E,BU,E,BR,BU;"+
				"E,E,E,SW,E,E,SW,E,E,E,E,BU,BU;"+
				"BR,BR,E,E,E,BU,BR,BU,E,E,E,BR,BR;"+
				"E,E,E,E,BR,BU,BU,BU,BR,E,E,E,E;"+
				"SW,BR,BR,E,BR,BU,BU,BU,BR,E,BR,BR,SW;"+
				"SW,SW,SW,E,E,E,BU,E,E,E,SW,SW,SW;"+
				"E,E,E,E,BR,E,E,E,BR,E,E,E,E;"+
				"E,BR,E,E,E,BR,E,BR,E,E,E,E,E;"+
				"E,BR,BR,E,E,E,E,E,E,E,BR,BR,BU;"+
				"E,E,E,E,E,BR,BR,BR,E,E,BU,BU,BU;"+
				"P1,P2,BR,E,E,BR,EA,BR,E,E,BR,BU,BU;"
			);

			descriptions.Add (
				"E,E,E,E,E,E,E,SW,SW,E,E,E,E;"+
				"E,SW,SW,SW,SW,E,E,E,E,E,SW,E,E;"+
				"E,SW,E,E,E,BU,E,E,E,SW,SW,E,E;"+
				"E,E,E,E,BU,SW,E,E,E,E,SW,E,E;"+
				"E,E,E,BU,SW,SW,E,E,E,E,E,SW,E;"+
				"E,E,E,SW,SW,E,E,E,SW,E,E,E,E;"+
				"E,E,E,E,SW,E,SW,SW,SW,SW,E,SW,E;"+
				"SW,E,E,E,E,E,SW,SW,BU,E,E,SW,E;"+
				"E,E,E,E,E,E,SW,BU,E,E,SW,E,E;"+
				"E,E,SW,E,E,E,BU,E,E,SW,E,E,E;"+
				"E,E,SW,SW,E,E,E,E,E,SW,E,SW,SW;"+
				"E,E,E,E,E,BR,BR,BR,E,E,E,E,E;"+
				"SW,SW,E,P1,E,BR,EA,BR,E,P2,E,E,E;"
			);

			descriptions.Add (
				"E,E,BR,E,E,BR,E,E,E,BR,E,E,E;"+
				"BU,BR,BR,BR,E,BR,E,SW,E,BR,E,E,E;"+
				"BU,BU,BU,BU,E,E,E,BR,E,E,E,E,BR;"+
				"BU,WA,WA,WA,WA,WA,WA,WA,WA,WA,WA,E,WA;"+
				"E,E,BR,E,E,E,E,E,E,E,E,E,E;"+
				"E,E,E,BR,E,E,BR,BR,BR,BR,BR,SW,SW;"+
				"BR,BR,E,BR,E,E,BR,BR,BR,BU,E,E,E;"+
				"E,E,E,SW,E,E,E,BU,BU,BU,BU,E,E;"+
				"WA,WA,E,WA,WA,WA,WA,WA,E,WA,WA,WA,WA;"+
				"BU,BU,E,BR,E,E,E,E,E,E,E,E,E;"+
				"BU,BU,E,BR,E,E,E,E,E,SW,BR,BR,E;"+
				"BU,E,E,BR,E,BR,BR,BR,E,E,E,BR,E;"+
				"E,E,E,E,P1,BR,EA,BR,P2,BR,E,E,E;"
			);

			descriptions.Add (
				"E,E,E,E,E,SW,E,BR,E,BR,BR,E,E;"+
				"E,BR,BR,BR,BR,BR,E,BR,E,E,E,E,E;"+
				"E,E,E,BR,E,BR,E,BR,BR,E,BU,BU,BU;"+
				"E,BR,E,E,E,E,E,SW,E,BU,BU,BU,BU;"+
				"E,BR,BR,BR,BR,BR,SW,BR,E,BU,BU,BR,SW;"+
				"E,E,E,BR,E,E,BU,BU,BU,BU,BU,E,BR;"+
				"E,E,E,SW,E,E,BU,BU,BU,BU,BU,BR,E;"+
				"SW,BR,E,BU,BU,BU,BU,SW,BU,BU,BU,BR,E;"+
				"E,BR,E,BU,BU,BU,BU,E,E,E,E,BR,E;"+
				"E,BR,BU,BU,E,E,E,E,SW,BR,BR,BR,E;"+
				"E,BR,BU,BU,E,E,E,E,E,BR,E,BR,E;"+
				"E,E,BU,BU,E,BR,BR,BR,E,E,E,BR,E;"+
				"E,BR,BU,BU,P1,BR,EA,BR,P2,E,E,E,E;"
			);

			descriptions.Add (
				"E,BR,E,BR,E,BR,E,BR,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,BR,E,BR,E,BR,E,BR,E;"+
				"E,SW,E,SW,E,SW,E,SW,E,SW,E,SW,E;"+
				"E,E,E,E,E,E,E,E,E,E,E,E,E;"+
				"E,E,E,E,SW,E,SW,E,SW,E,E,E,E;"+
				"BU,BU,E,E,BR,E,E,E,BR,E,E,BU,BU;"+
				"BU,BU,BU,BU,BR,E,BU,E,BR,BU,BU,BU,BU;"+
				"BU,BU,BU,BU,BU,BU,BU,BU,BU,BU,BU,BU,BU;"+
				"E,E,E,E,BR,BU,BU,BU,BR,E,E,E,E;"+
				"BR,E,BR,BR,E,E,BU,E,E,BR,BR,E,BR;"+
				"E,BR,E,BR,E,E,E,E,E,BR,E,BR,E;"+
				"E,BR,E,BR,E,BR,BR,BR,E,BR,E,BR,E;"+
				"E,E,E,E,P1,BR,EA,BR,P2,E,E,E,E;"
			);

			descriptions.Add (
				"E,E,E,E,SW,E,E,E,E,E,E,E,E;"+
				"E,SW,E,E,SW,E,E,SW,SW,E,E,E,E;"+
				"E,SW,E,E,SW,E,E,E,SW,E,SW,SW,BU;"+
				"E,SW,E,E,SW,SW,SW,E,BR,E,SW,E,E;"+
				"E,BR,E,E,E,E,E,E,SW,SW,SW,E,E;"+
				"BU,SW,SW,E,SW,BR,SW,BR,BR,E,E,E,E;"+
				"E,E,SW,BU,SW,BU,E,E,BR,E,E,SW,BU;"+
				"E,E,SW,E,E,BU,E,E,SW,E,E,SW,E;"+
				"E,E,BR,E,E,SW,E,E,SW,SW,BR,SW,E;"+
				"BU,SW,SW,BU,BU,BU,BR,SW,SW,E,BR,SW,E;"+
				"E,E,BR,E,E,E,E,E,BU,BU,E,BR,E;"+
				"E,E,SW,E,E,BR,BR,BR,E,BU,E,BR,E;"+
				"E,E,SW,E,P1,BR,EA,BR,P2,SW,E,BR,E;"
			);

/*
 * 
 * Stage description:
 * ; - üres sor
 * BR - tégla 
 * SW - acélfal
 * WA - víz
 * BU - bokor
 * IF - jég
 * E - üres
 * EA - EAGLE
 * P1 - player1 kezdeti pozició
 * P2 - player2 kezdeti pozició
 * Az elemek vesszővel elválasztva
 * Pl.:
 * E, BR, IF, BR, ...
 * 13 db egy sorban végén ;
 * 13 sor
 * 
 */
            }

        public static string Get(int idx)
        {
            return descriptions.ElementAt(idx);
        }

    }
}
