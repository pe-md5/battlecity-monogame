﻿using BattleCity.AI;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Utils
{


    public static class Settings
    {
        /// <summary>
        /// Player related settings, coordinates, speed, animation speed, etc.
        /// </summary>
        /// 

        public static readonly Color Player1Color = new Color(86, 110, 77);
        public static readonly Color Player2Color = new Color(255, 182, 0);
		public static readonly float PlayerAnimationSecPerFrame = 0.2f;
		public static readonly float PlayerCornerTurnSpeed = 1.0f;
		public static readonly float PlayerCornerTurnLimit = 8.0f;
        public static readonly float InitialPlayerSpeed = 80;

        public static readonly float BulletSpeed = 200;

        public static readonly float BulletAnimationSecPerFrame = float.MaxValue;

        public static readonly Vector2 BulletPositionLeft = new Vector2(0, 19);
        public static readonly Vector2 BulletPositionRight = new Vector2(44, 19);
        public static readonly Vector2 BulletPositionUp = new Vector2(19, 0);
        public static readonly Vector2 BulletPositionDown = new Vector2(19, 44);

        public static readonly Vector2 BulletParticlePositionLeft = new Vector2(17, 4);
        public static readonly Vector2 BulletParticlePositionRight = new Vector2(0, 4);
        public static readonly Vector2 BulletParticlePositionUp = new Vector2(4, 17);
        public static readonly Vector2 BulletParticlePositionDown = new Vector2(4, 0);

        public static readonly float BulletParticleDelay = 16;

        // Stage related settings

        public static readonly Vector2 StagePosition = Vector2.Zero;
        public static readonly Vector2 StageElementSize = new Vector2(50, 50);
        public static readonly int StageBorderSize = 10;
        // public static readonly Vector2 EaglePosition = new Vector2(300, 600);
        public static readonly Cell EagleCellPosition = new Cell(12, 6);

        public static float WaterAnimationSecPerFrame = 0.2f;

        public static int NumberOfExplosionFrames = 12;
        public static float ExplosionAnimationSecPerFrame = 0.06f;

        // enemy related settings
        public static Color EasyEnemyColor = new Color(34, 40, 125);
        public static Color MediumEnemyColor = new Color(200, 0, 0);//128
        public static Color HardEnemyColor = new Color(15, 15, 15);

        // this is for random and pathfinding timeout calculation for each stage
        public static float InitialRandomTimeOut = 4.0f;
		public static float InitialPathFindingTimeOut = 1.0f;

        // this is for enemy spawning timeout calculation for each stage
        public static float InitialSpawnTimeOut = 3.0f;
        public static float SpawnTimeOutDecreaseValue = 0.08f;

        // this is for easy-medium-hard rates calculation for each stage
        // stageidx/35 felso egesz resze * 20 lesz az ellenfelek kozul nehez (medium vagy hard), 
        //  es azon belul ezen aranyok szerint szamolunk, az ellenfelek kozul nehez * medium a medium ellenfelek szama
        // majd az ellenfelek kozul nehez - medium ellenfelek szama a hard ellenfelek szama
        public static float MediumEnemyRatioBetween1And10 = 1.0f;
        public static float MediumEnemyRatioBetween11And20 = 3/4f;
        public static float MediumEnemyRatioBetween21And30 = 2/4f;
        public static float MediumEnemyRatioBetween31And35 = 1/4;

		public static bool TESZT = false;

        public static int Port = 14242;
    }
}
