﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;

namespace BattleCity.Utils
{
    public static class Resources
    {
        public static SpriteFont Font;

        // Animatedspriteimageket ki kell törölni az összes helyen, helyettük csak a texturát lehet használni

        public static SimpleSpriteImage[] BulletImages;

        public static AnimatedSpriteImage WaterImage;
        public static SimpleSpriteImage[] BrickWallImages;
        public static SimpleSpriteImage[] SteelWallImages;
        public static SimpleSpriteImage BushImage;
        public static SimpleSpriteImage[] EagleImages;
        public static SimpleSpriteImage IcefieldImage;

        public static SimpleSpriteImage StageBackground;
        public static SimpleSpriteImage StageBorderHorizontal;
        public static SimpleSpriteImage StageBorderVertical;
        public static SimpleSpriteImage[] ButtonImages;

        public static SimpleSpriteImage[] PowerUpImages;

        public static SimpleSpriteImage TitleImage;

        public static Texture2D ExplosionTexture;
        public static AnimatedSpriteImage ProtectorFieldImages;

        public static SimpleSpriteImage IndicatorBackground;
        public static SimpleSpriteImage[] PrevButtonImages;
        public static SimpleSpriteImage ListBackgroundImage;
        public static SimpleSpriteImage[] NextButtonImages;
        public static SimpleSpriteImage[] ListItemImages;

        public static void LoadContent(ContentManager content)
        {
            TankTextures = new Texture2D[8];
            TankTextures[0] = content.Load<Texture2D>(@"Tank\tank_moving_up");
            TankTextures[1] = content.Load<Texture2D>(@"Tank\tank_moving_down");
            TankTextures[2] = content.Load<Texture2D>(@"Tank\tank_moving_left");
            TankTextures[3] = content.Load<Texture2D>(@"Tank\tank_moving_right");
            TankTextures[4] = content.Load<Texture2D>(@"Tank\tank_idle_up");
            TankTextures[5] = content.Load<Texture2D>(@"Tank\tank_idle_down");
            TankTextures[6] = content.Load<Texture2D>(@"Tank\tank_idle_left");
            TankTextures[7] = content.Load<Texture2D>(@"Tank\tank_idle_right");


            BulletImages = new SimpleSpriteImage[4];
            BulletImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"Bullet\bullet_up"));
            BulletImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"Bullet\bullet_down"));
            BulletImages[2] = new SimpleSpriteImage(content.Load<Texture2D>(@"Bullet\bullet_left"));
            BulletImages[3] = new SimpleSpriteImage(content.Load<Texture2D>(@"Bullet\bullet_right"));

            WaterImage = new AnimatedSpriteImage(content.Load<Texture2D>(@"Stage\water"), 10, Settings.WaterAnimationSecPerFrame);
            BrickWallImages = new SimpleSpriteImage[5];
            BrickWallImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\brick_0"));
            BrickWallImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\brick_1"));
            BrickWallImages[2] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\brick_2"));
            BrickWallImages[3] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\brick_3"));
            BrickWallImages[4] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\brick_4"));
            SteelWallImages = new SimpleSpriteImage[5];
            SteelWallImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\steelwall_0"));
            SteelWallImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\steelwall_1"));
            SteelWallImages[2] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\steelwall_2"));
            SteelWallImages[3] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\steelwall_3"));
            BushImage = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\bush"));

            EagleImages = new SimpleSpriteImage[4]; 
            EagleImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\eagle_0"));
            EagleImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\eagle_1"));
            EagleImages[2] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\eagle_2"));
            EagleImages[3] = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\eagle_3"));

            IcefieldImage = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\icefield"));

            StageBackground = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\background"));
            StageBorderHorizontal = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\border_horizontal"));
            StageBorderVertical = new SimpleSpriteImage(content.Load<Texture2D>(@"Stage\border_vertical"));

            ButtonImages = new SimpleSpriteImage[2];
            ButtonImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\button_background"));
            ButtonImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\button_background_clicked"));

            Font = content.Load<SpriteFont>(@"GUI\Font");

            TitleImage = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\title"));

            ExplosionTexture = content.Load<Texture2D>(@"Effect\explosion");

            ProtectorFieldImages = new AnimatedSpriteImage(content.Load<Texture2D>(@"Effect\protectorfield"), 2, 0.1f);
            
            PowerUpImages = new SimpleSpriteImage[5];
            PowerUpImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"PowerUps\powerup_tank"));
            PowerUpImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"PowerUps\powerup_star"));
            PowerUpImages[2] = new SimpleSpriteImage(content.Load<Texture2D>(@"PowerUps\powerup_bomb"));
            PowerUpImages[3] = new SimpleSpriteImage(content.Load<Texture2D>(@"PowerUps\powerup_clock"));
            PowerUpImages[4] = new SimpleSpriteImage(content.Load<Texture2D>(@"PowerUps\powerup_shield"));

            PrevButtonImages = new SimpleSpriteImage[2];
            PrevButtonImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\back_button"));
            PrevButtonImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\back_button_clicked"));
            ListBackgroundImage = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\list_background"));
            NextButtonImages = new SimpleSpriteImage[2];
            NextButtonImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\next_button"));
            NextButtonImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\next_button_clicked"));
            ListItemImages = new SimpleSpriteImage[3];
            ListItemImages[0] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\listitem"));
            ListItemImages[1] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\listitem_hover"));
            ListItemImages[2] = new SimpleSpriteImage(content.Load<Texture2D>(@"GUI\listitem_selected"));
        }

        public static Texture2D[] TankTextures { get; set; }
    }
}
