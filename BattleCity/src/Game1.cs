﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using BattleCity.GUI;
using System.Threading;
using BattleCity.Utils;
#endregion

namespace BattleCity
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    /// 
    public class Game1 : Game
    {
        GameState state = GameState.MainMenu;

        public GameState State
        {
            get { return state; }
            set { state = value; }
        }

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        LoadingScreen loadingScreen;

        Controller controller;

        MainMenu mainMenu;

        MultiplayerMenu multiMenu;

        PauseScreen pauseScreen;


        static Game1 instance;

        public static Game1 Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Game1();
                }
                return instance;
            }
        }

        private Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
			graphics.PreferredBackBufferWidth = 670;
			graphics.PreferredBackBufferHeight = 670;
			graphics.IsFullScreen = false;
			graphics.ApplyChanges ();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Resources.LoadContent(this.Content);

            controller = Controller.Instance;

            //controller.LoadStage("");
            mainMenu = new MainMenu(new Rectangle(0, 0, 670, 670));
            pauseScreen = new PauseScreen(new Rectangle(0, 0, 670, 670));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
        {
            switch (state)
            {
                case GameState.MPClientDiscover:
                    DiscoverMenu.Instance.Update(gameTime);
                    break;
                case GameState.MainMenu:
                    mainMenu.Update(gameTime);
                    break;
                case GameState.MPMenu:
                    if (multiMenu == null)
                    {
                        multiMenu = new MultiplayerMenu(new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height));
                    }
                    else
                    {
                        multiMenu.Update(gameTime);
                    }
                    break;
				case GameState.InSP:
                    controller.Update(gameTime);
                    break;
				case GameState.InMP:
                    controller.Update(gameTime);
					break;
                case GameState.SPGameOver:
                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen("Game over", graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 2);
                        loadingScreen.Loaded += loadingScreen_Loaded;
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                    }
                    break;
                case GameState.SPCompleted:
                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen("Congratulation...", graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 2);
                        loadingScreen.Loaded += loadingScreen_Loaded;
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                    }
                    break;
                case GameState.MPCompleted:
                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen("Congratulation...", graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 2);
                        loadingScreen.Loaded += loadingScreen_Loaded;
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                    }
                    break;
                case GameState.SPPause:
                    controller.Pause();
                    pauseScreen.Update(gameTime);
                    break;
				case GameState.MPPause:
                    controller.Update(gameTime);
                    pauseScreen.Update(gameTime);
                    break;
                case GameState.SPLoading:
                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen("Stage " + (controller.StageIndex + 1), graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 2);
                        loadingScreen.Loaded += loadingScreen_Loaded;
                        controller.LoadStage();
                        controller.Pause();
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                    }
                    
					break;

                case GameState.MPServerStart:

                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen(
                            "Waiting for partner..." +
                            "\nStage " + (controller.StageIndex + 1) +
                            "\nPress esc to abort."
                            , graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 0);
                        controller.StartServer();
                        controller.LoadStage();
                        controller.Pause();
                        this.Window.Title = "BattleCity - Server";
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                        if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                        {
                            controller.StopServer();
                            state = GameState.MainMenu;
                        }
                        else if (Controller.Instance.ClientConnected)
                        {
                            state = GameState.InMP;
                            controller.Resume();
                            loadingScreen = null;
                        }
                    }
                    break;
				case GameState.MPLoading:
                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen("Stage " + (controller.StageIndex + 1), graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 2);
                        loadingScreen.Loaded += loadingScreen_Loaded;
                        controller.LoadStage();
                        controller.Pause();
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                    }

				break;
                case GameState.MPGameOver:
                    if (loadingScreen == null)
                    {
                        loadingScreen = new LoadingScreen("Game over", graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height, Resources.Font, 2);
                        loadingScreen.Loaded += loadingScreen_Loaded;
                    }
                    else
                    {
                        loadingScreen.Update(gameTime);
                    }
                    break;
            }
            
            base.Update(gameTime);
		}

        public void ClearLoadingScreen()
        {
            loadingScreen = null;
        }

		void loadingScreen_Loaded(object sender, EventArgs e)
		{
			if (state == GameState.SPLoading) {
                state = GameState.InSP;
                controller.Resume();
			} 
            else if (state == GameState.SPGameOver ||
                state == GameState.SPCompleted)
            {
                state = GameState.MainMenu;
            }
            else if (state == GameState.MPGameOver)
            {
                Controller.Instance.StopClient();
                Controller.Instance.StopServer();
            }
            else if (state == GameState.MPCompleted)
            {
                Controller.Instance.StopClient();
                Controller.Instance.StopServer();
            }
            else if (state == GameState.MPLoading)
            {
                if (Controller.Instance.IsClient && !Controller.Instance.ConnectedToServer)
                {
                    this.Window.Title = "BattleCity - Client";
                    Controller.Instance.ConnectToServer(DiscoverMenu.Instance.SelectedIPAddress);
                }
                else
                {
                    state = GameState.InMP;
                }

                controller.Resume();
            }
            loadingScreen = null;
		}

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            switch (state)
            {
                case GameState.InSP:
                    controller.Draw(spriteBatch);
                    break;
                case GameState.MPClientDiscover:
                    DiscoverMenu.Instance.Draw(spriteBatch);
                    break;
				case GameState.InMP:
                    controller.Draw(spriteBatch);
                    break;
                case GameState.SPPause:
                    controller.Draw(spriteBatch);
                    pauseScreen.Draw(spriteBatch);
                    break;
                case GameState.MPMenu:
                    if (multiMenu != null)
                    {
                        multiMenu.Draw(spriteBatch);
                    }
                    break;
				case GameState.MPPause:
                    controller.Draw(spriteBatch);
                    pauseScreen.Draw(spriteBatch);
                    break;
                case GameState.MainMenu:
                    mainMenu.Draw(spriteBatch);
                    break;
            }

            if (loadingScreen != null)
            {
                loadingScreen.Draw(spriteBatch);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }


        protected override void OnExiting(Object sender, EventArgs args)
        {
            base.OnExiting(sender, args);

            if (Controller.Instance.IsClient)
            {
                Controller.Instance.StopClient();
            }
            else if (Controller.Instance.IsServer)
            {
                Controller.Instance.StopServer();
            }
        }
    }
}
