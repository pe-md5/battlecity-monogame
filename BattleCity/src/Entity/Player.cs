﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BattleCity.Sprite;
using BattleCity.Effect;
using BattleCity.Network.Events;
using BattleCity.AI;
using BattleCity.StageElements;
using BattleCity.Utils;

namespace BattleCity.Entity
{

    public class Player : MultiImageSprite
    {
		int playerNumber = 1;
        Bullet bullet;
        int bulletImageIdx;
        float speed;
        float bulletSpeed;
        Vector2 previousPosition;
        public event EventHandler Shoot;
        bool protect = true;
        BasicSprite protectorField;
        float protectTime = 1.5f;
        float elapsedTime = 0;
        bool slide = false;
        bool canShootSteelWall = false;
        int starPowerUpCount = 0;
        int lives = 3;
        bool hide = false;
        float broadCastTimeOut = 1 / 30;
        float elapsedGameTimeForBroadCast = 0.0f;

        public bool IsHide
        {
            get { return hide; }
        }

		public int PlayerNumber {
			get { return playerNumber; }
		}

        public Bullet Bullet {
            get { return bullet; }
        }

        public int Lives {
            get { return lives; }
            set { lives = value; }
        }

		public float Speed {
			get { return speed; }
		}


        Directions direction;

		public Directions Direction {
			get { return direction; }
		}

        public Player(int playerNumber, float speed, float bulletSpeed, Vector2 position, Color color)
            : base(Helper.GetTankAnimatedSpriteImageArray(), position, color)
        {
			this.playerNumber = playerNumber;
            this.direction = Directions.UP;
            this.speed = speed;
            this.bulletSpeed = bulletSpeed;

            var pFieldSize = Resources.ProtectorFieldImages.GetCurrentImageSize();
            var playerSize = GetSpriteImage().GetCurrentImageSize();

            protectorField = new BasicSprite(Resources.ProtectorFieldImages, new Vector2(position.X + (playerSize.X - 
                pFieldSize.X)/2, position.Y + (playerSize.Y - pFieldSize.Y)/2));
        }


        public bool Firing
        {
            get { return bullet != null && !bullet.ExplosionEnded; }
        }

        public bool IsMovingAnimationOn
        {
            get { return ImageIdx < 4; }
        }

        public override void Update(GameTime gameTime)
        {

            previousPosition = position;

            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            float deltaT = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!CheckCollision())
            {
                slide = false;
            }

            elapsedGameTimeForBroadCast += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                direction = Directions.UP;
                position.Y -= deltaT * speed;
                SetImagesAccordingToDirection(true);

                if (!Controller.Instance.IsSingle && elapsedGameTimeForBroadCast >= broadCastTimeOut)
                {
                    UpdateMovingToPartner();
                    elapsedGameTimeForBroadCast = 0.0f;
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                direction = Directions.DOWN;
                position.Y += deltaT * speed;
                SetImagesAccordingToDirection(true);

                if (!Controller.Instance.IsSingle && elapsedGameTimeForBroadCast >= broadCastTimeOut)
                {
                    UpdateMovingToPartner();
                    elapsedGameTimeForBroadCast = 0.0f;
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                direction = Directions.LEFT;
                position.X -= deltaT * speed;
                SetImagesAccordingToDirection(true);

                if (!Controller.Instance.IsSingle && elapsedGameTimeForBroadCast >= broadCastTimeOut)
                {
                    UpdateMovingToPartner();
                    elapsedGameTimeForBroadCast = 0.0f;
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                direction = Directions.RIGHT;
                position.X += deltaT * speed;
                SetImagesAccordingToDirection(true);

                if (!Controller.Instance.IsSingle && elapsedGameTimeForBroadCast >= broadCastTimeOut)
                {
                    UpdateMovingToPartner();
                    elapsedGameTimeForBroadCast = 0.0f;
                }
            }
            else
            {
                SetImagesAccordingToDirection(false);

                if (!Controller.Instance.IsSingle && elapsedGameTimeForBroadCast >= broadCastTimeOut)
                {
                    UpdateMovingToPartner();
                    elapsedGameTimeForBroadCast = 0.0f;
                }
            }




            if (Keyboard.GetState().IsKeyDown(Keys.Space) && bullet == null)
            {
                TryToShoot();

                if (!Controller.Instance.IsSingle)
                {
                    UpdateShootingToPartner();
                }
            }
            

            if (slide)
            {
                switch (direction)
                {
                    case Directions.UP:
                        position.Y -= this.speed / 3 * deltaT;
                        break;
                    case Directions.DOWN:
                        position.Y += this.speed / 3 * deltaT;
                        break;
                    case Directions.LEFT:
                        position.X -= this.speed / 3 * deltaT;
                        break;
                    case Directions.RIGHT:
                        position.X += this.speed / 3 * deltaT;
                        break;
                }

                SetImagesAccordingToDirection(false);

                if (!Controller.Instance.IsSingle)
                {
                    UpdateMovingToPartner();
                }
            }

            base.Update(gameTime);

        }

        private void UpdateShootingToPartner()
        {
            Controller.Instance.SendNetworkEvent(new NetworkPlayerShootingEvent() { id = playerNumber });
        }

        public bool IsProtected
        {
            get { return protect; }
        }

        public void UpdateProtectorField(GameTime gameTime)
        {
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (protect && elapsedTime >= protectTime)
            {
                protect = false;
                elapsedTime = 0;
				protectTime = 30.0f;

                if (!Controller.Instance.IsSingle)
                {
                    UpdateProtectorFieldToPartner();
                }
            }

            if (protect)
            {
                protectorField.Update(gameTime);
                RecalculateProtectorFieldPosition();
            }
        }

        private void SetImagesAccordingToDirection(bool isMoving)
        {
            int baseIdx = 4;

            if (isMoving)
            {
                baseIdx = 0;
            }

            switch (direction)
            {
                case Directions.UP:
                    SetCurrentImage(baseIdx);
                    bulletImageIdx = 0;
                    break;
                case Directions.DOWN:
                    SetCurrentImage(baseIdx + 1);
                    bulletImageIdx = 1;
                    break;
                case Directions.LEFT:
                    SetCurrentImage(baseIdx + 2);
                    bulletImageIdx = 2;
                    break;
                case Directions.RIGHT:
                    SetCurrentImage(baseIdx + 3);
                    bulletImageIdx = 3;
                    break;
            }
        }

        public void UpdateBullet(GameTime gameTime)
        {
            if (bullet != null)
            {
                bullet.Update(gameTime);

                if (bullet.ExplosionEnded)
                {
                    bullet = null;
                }
            }
        }

        private void RecalculateProtectorFieldPosition()
        {
            var pFieldSize = Resources.ProtectorFieldImages.GetCurrentImageSize();
            var playerSize = GetSpriteImage().GetCurrentImageSize();

            protectorField.Position = new Vector2(position.X + (playerSize.X -
                pFieldSize.X) / 2, position.Y + (playerSize.Y - pFieldSize.Y) / 2);
        }

        void bullet_CollisionWith(BasicSprite other)
        {
            if (other is Enemy)
            {
                ((Enemy)other).TryToKillMe();
            }
            else if (other is Player)
            {
                ((Player)other).TryToKillMe();
            }
            else if (other is SteelWall && canShootSteelWall)
            {
                var bw = (SteelWall)other;
                bw.DecrementShotsCount(1);

                if (!Controller.Instance.IsSingle)
                {
                    Controller.Instance.UpdateStageEventToPartner(other, 1);
                }
            }

            if (bullet.Explosion && !Controller.Instance.IsSingle)
            {
                UpdatePBulletCollideToPartner();
            }
        }

        public override void Draw(SpriteBatch batch)
        {

            base.Draw(batch);

            if (protect)
            {
                protectorField.Draw(batch);
            }
        }

        public void DrawBullet(SpriteBatch batch)
        {
            if (bullet != null)
            {
                bullet.Draw(batch);
            }
        }

        private Vector2 BulletPosition
        {
            get
            {
                Vector2 bulletPos = position;

                switch (direction)
                {
                    case Directions.UP:
                        bulletPos += Settings.BulletPositionUp;
                        break;
                    case Directions.DOWN:
                        bulletPos += Settings.BulletPositionDown;
                        break;
                    case Directions.LEFT:
                        bulletPos += Settings.BulletPositionLeft;
                        break;
                    case Directions.RIGHT:
                        bulletPos += Settings.BulletPositionRight;
                        break;
                }

                return bulletPos;
            }
        }

        private void TryToShoot()
        {
            if (bullet == null)
            {
                    bullet = new Bullet(Resources.BulletImages[bulletImageIdx], bulletSpeed, direction, BulletPosition);

                    if (Shoot != null)
                    {
                        Shoot(this, null);
                    }

                    bullet.CollisionWith += bullet_CollisionWith;
            }
        }

        public override void OnCollisionWith(BasicSprite other)
        {
            if (other is Icefield)
            {
                slide = true;
            }
            else if (other is PowerUp)
            {
                other.Destroy();
                SetPropertyBasedOnPowerUp(other as PowerUp);

                if (!Controller.Instance.IsSingle)
                {
                    var pu = other as PowerUp;
                    Controller.Instance.SendNetworkEvent(new NetworkPowerUpGainEvent() { powerUpidx = pu.Id, powerUptype = pu.PType });
                }
            }
            else if (other is Bush)
            {
                var bushRect = other.GetCollider();
                var bushPos = other.Position;
                if (bushPos.X - 5 <= position.X && bushPos.Y - 5 <= position.Y
                    && position.X + GetCollider().Width <= bushPos.X + bushRect.Width + 5
                    && position.Y + GetCollider().Height <= bushPos.Y + bushRect.Height + 5)
                {
                    hide = true;
                }
                else
                {
                    hide = false;
                }

                if (!Controller.Instance.IsSingle)
                {
                    UpdateHideToPartner();
                }
            }
            else
			{
				bool is_moving = mayMoveBetweenWalls (other);
                position = previousPosition;
                SetImagesAccordingToDirection(is_moving);
            }

            base.OnCollisionWith(other);

        }

		// Átfér-e két fal között.
		// Ha a csak néhány pixel kell ahhoz, hogy beforduljon egy sarkon, akkor ne akadjon el.
		private bool mayMoveBetweenWalls(BasicSprite other)
		{
			float e_size = (float)Settings.StageElementSize.X;
			float pos_x = (float)(position.X - Settings.StageBorderSize);
			float pos_y = (float)(position.Y - Settings.StageBorderSize);

			// pixelek alapján melyik i,j cellá(k)ban helyezkedhet el
			float ipx = pos_x / e_size;
			float jpx =  pos_y / e_size;

			float d1, d2, corner, correction = Settings.PlayerCornerTurnSpeed * -1.0f;
			int axis, head;

			Stage stage = Controller.Instance.CurrentStage;
			BasicSprite path;

			if (direction == Directions.LEFT || direction == Directions.RIGHT)
			{	// horizontális irányt vizsgálunk
				axis = (int)Math.Round (ipx); // tengely oszlop
				if (direction == Directions.LEFT) {
					if (axis == 0) { // ki szeretne menni a pályáról
						return false;
					} else {
						axis--;
					}
				} else if (axis == 12) { // ki szeretne menni a pályáról
					return false;
				} else {
					axis++;
				}

				head = (int)Math.Floor (jpx); // felső cella sora
				corner = ((float)(head + 1)) * e_size;

				d1 = Math.Abs (pos_y - corner);
				d2 = Math.Abs (pos_y - corner + GetSize().Y);
				if (d1 < d2 && d1 < Settings.PlayerCornerTurnLimit) { // alsó cellába menne
					correction *= -1.0f;
					head++;
				} else if(d2 > Settings.PlayerCornerTurnLimit) { // felsőbe, de nagy felülettel a másikban
					return false;
				}

				path = stage.StageElements [head, axis]; // az a cella, ahova menne
				if (path != null && !path.IsDestroyed ()) { // de hát ott fal van
					return false;
				} else {
					previousPosition.Y += correction;
					return true;
				}

			}
			else
			{	// vertikális irányt vizsgálunk
				axis = (int)Math.Round (jpx); // tengely sor
				if (direction == Directions.UP) {
					if (axis == 0) { // ki szeretne menni a pályáról
						return false;
					} else {
						axis--;
					}
				} else if (axis == 12) { // ki szeretne menni a pályáról
					return false;
				} else {
					axis++;
				}

				head = (int)Math.Floor (ipx); // bal cella oszlopa
				corner = ((float)(head + 1)) * e_size;

				d1 = Math.Abs (pos_x - corner);
				d2 = Math.Abs (pos_x - corner + GetSize().X);
				if (d1 < d2 && d1 < Settings.PlayerCornerTurnLimit) { // jobb oldali cellába menne
					correction *= -1.0f;
					head++;
				} else if (d2 > Settings.PlayerCornerTurnLimit) { // bal, de nagy felülettel a másikba
					return false;
				}

				path = stage.StageElements [axis, head];
				if (path != null && !path.IsDestroyed ()) { // de hát ott fal van
					return false;
				} else {
					previousPosition.X += correction;
					return true;
				}

			}
		}

        private void SetPropertyBasedOnPowerUp(PowerUp other)
        {
            switch (other.PType)
            {
                case PowerUp.PTypes.SHIELD:
                    protect = true;
                    elapsedTime = 0;
                    if(!Controller.Instance.IsSingle) 
                    {
                        UpdateProtectorFieldToPartner();
                    }
                    break;
                case PowerUp.PTypes.STAR:
                    starPowerUpCount++;
                    switch (starPowerUpCount)
                    {
                        case 1:
                            // shoot faster
                            bulletSpeed *= 1.5f;
                            break;
                        case 2:
                            // move faster
                            speed *= 1.5f;
                            break;
                        case 3:
                            canShootSteelWall = true;
                            break;
                    }
                    break;
                case PowerUp.PTypes.TANK:
                    lives++;
                    break;
            }
        }

        public void TryToKillMe()
        {
            if (!protect)
            {
                if (!Settings.TESZT)
                {
                    lives--;
                }
            }

            if(lives > 0 && !protect)
            {

                protect = true;
                elapsedTime = 0;
				protectTime = 1.5f;

                //if(!Controller.Instance.IsSingle) 
                //{
                //    UpdateProtectorFieldToPartner();
                //}
            }

            if (lives == 0)
            {
                this.Destroy();
            }

            if (!Controller.Instance.IsSingle)
            {
                var evnt = new NetworkPlayerDamagedEvent();
                evnt.id = playerNumber;
                Controller.Instance.SendNetworkEvent(evnt);
            }
        }

        public void UpdateDamagedFromPartner()
        {
            if (!protect)
            {
                if (!Settings.TESZT)
                {
                    lives--;
                }
            }

            if (lives > 0 && !protect)
            {

                protect = true;
                elapsedTime = 0;
                protectTime = 1.5f;
            }

            if (lives == 0)
            {
                this.Destroy();
            }
        }

        /// <summary>
        /// Visszaadja azokat a cellapoziciókat, amiken a player épp tartózkodik.
        /// </summary>
        /// <returns></returns>
        public List<Vector2> GetCellPositions()
        {
            var list = new List<Vector2>();
			
			float e_size = (float)Settings.StageElementSize.X;
			float pos_x1 = (float)(position.X - Settings.StageBorderSize);
			float pos_x2 = pos_x1 + GetSize ().X;
			float pos_y1 = (float)(position.Y - Settings.StageBorderSize);
			float pos_y2 = pos_y1 + GetSize ().Y;

			int i1 = (int)Math.Floor (pos_x1 / e_size);
			int j1 = (int)Math.Floor (pos_y1 / e_size);
			int i2 = (int)Math.Floor (pos_x2 / e_size);
			int j2 = (int)Math.Floor (pos_y2 / e_size);

			list.Add (new Vector2 (j1, i1));
			if (i1 != i2 && i2 <= 12) {
				list.Add (new Vector2 (j1, i2));
			}

			if (j1 != j2 && j2 <= 12) {
				list.Add (new Vector2 (j2, i1));
			}

			if (i1 != i2 && j1 != j2 && i2 <= 12 && j2 <= 12) {
				list.Add (new Vector2 (j2, i2));
			}

            return list;
        }

        public void UpdateMovingFromPartner(NetworkPlayerMovingEvent e)
        {
            direction = e.direciton;
            position = e.position;
            SetImagesAccordingToDirection(e.moving);
        }

        public void UpdateMovingToPartner()
        {
            var evnt = new NetworkPlayerMovingEvent();
            evnt.direciton = direction;
            evnt.moving = IsMovingAnimationOn;
            
            evnt.position = CheckCollision() ?  previousPosition : position;
            evnt.id = playerNumber;
            Controller.Instance.SendNetworkEvent(evnt);
        }


        public void UpdateShootingFromPartner(Network.Events.NetworkPlayerShootingEvent e)
        {
            TryToShoot();
        }

        public void UpdatePBulletCollideFromPartner()
        {
            if (bullet != null)
            {
                bullet.Explode();
            }
        }

        public void UpdatePBulletCollideToPartner()
        {
            Controller.Instance.SendNetworkEvent(new NetworkPBulletCollideEvent() { id = playerNumber });
        }


        public void UpdateProtectorFieldFromPartner(NetworkPlayerProtectEvent e)
        {
            protect = e.protect;
            elapsedTime = 0;
        }

        public void UpdateProtectorFieldToPartner()
        {
            Controller.Instance.SendNetworkEvent(new NetworkPlayerProtectEvent() { id = playerNumber, protect = protect });
        }

        public void UpdateHideFromPartner(NetworkPlayerHideEvent e)
        {
            hide = e.hide;
        }

        public void UpdateHideToPartner()
        {
            Controller.Instance.SendNetworkEvent(new NetworkPlayerHideEvent() { id = playerNumber, hide = hide });
        }
    }
}
