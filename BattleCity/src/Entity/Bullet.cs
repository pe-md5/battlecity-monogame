﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using BattleCity.Sprite;
using BattleCity.Utils;
using BattleCity.StageElements;

namespace BattleCity.Entity
{
    
    public class Bullet : BasicSprite
    {

        Directions direction;
        float speed;
        bool explode = false;
        AnimatedSpriteImage explosionAnimation;

		public bool Exploding
		{
			get {
				return explode;
			}
		}

        public bool ExplosionEnded
        {
            get;
            private set;
        }

        public Bullet(SimpleSpriteImage simage, float speed, Directions direction, Vector2 position)
            : base(simage, position, Color.White)
        {

            this.direction = direction;
            this.speed = speed;
            ExplosionEnded = false;
            explosionAnimation = new AnimatedSpriteImage(Resources.ExplosionTexture, Settings.NumberOfExplosionFrames, Settings.ExplosionAnimationSecPerFrame);
        }

        public override void Update(GameTime gameTime)
        {
            if (!explode)
            {
                float deltaT = (float)gameTime.ElapsedGameTime.TotalSeconds;

                switch (direction)
                {
                    case Directions.UP:
                        position.Y -= speed * deltaT;
                        break;
                    case Directions.DOWN:
                        position.Y += speed * deltaT;
                        break;
                    case Directions.LEFT:
                        position.X -= speed * deltaT;
                        break;
                    case Directions.RIGHT:
                        position.X += speed * deltaT;
                        break;
                }
            }
            else
            {
                var image = (GetSpriteImage() as AnimatedSpriteImage);
                if (image != null)
                {
                    if (image.FrameCount - 1 == image.CurrentFrame)
                    {
                        ExplosionEnded = true;
                    }
                }

            }


            base.Update(gameTime);
        }

        public override void OnCollisionWith(BasicSprite other)
        {

            if (!explode)
            {
                if (other is BrickWall)
                {
                    var bw = (BrickWall)other;
                    bw.DecrementShotsCount(1);
                    if (!Controller.Instance.IsSingle)
                    {
                        Controller.Instance.UpdateStageEventToPartner(other, 1);
                    }
                    explode = true;
                }
                else if (other is Eagle)
                {
                    var eg = (Eagle)other;
                    eg.DecrementShotsCount(1);
                    explode = true;
                    if(!Controller.Instance.IsSingle)
                    {
                        Controller.Instance.UpdateStageEventToPartner(other, 1);
                    }
                }
                else if (other is Bullet)
                {
                    var b = other as Bullet;
                    b.Explode();
                    explode = true;
                }
                else
                {
                    explode = true;
                }
            } 

            if (explode)
            {
                Explode();
            }

            base.OnCollisionWith(other);
        }

        public void Explode() 
        {
                explode = true;
                Destroy();
                var size = GetSpriteImage().GetCurrentImageSize();
                var explSize = explosionAnimation.GetCurrentImageSize();
                position = new Vector2(position.X - (explSize.X - size.X) / 2, position.Y - (explSize.Y - size.Y) / 2);

                switch (direction)
                {
                    case Directions.UP:
                        position.Y -= 20;
                        break;
                    case Directions.DOWN:
                        position.Y += 20 - size.Y;
                        break;
                    case Directions.LEFT:
                        position.X -= 20;
                        break;
                    case Directions.RIGHT:
                        position.X += 20 - size.X;
                        break;
                }

                SetImage(explosionAnimation);

                StopCollisionCheck();
            
        }

        public bool Explosion
        {
            get { return explode; }
        }
    }
}
