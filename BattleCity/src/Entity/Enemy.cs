﻿using BattleCity.Sprite;
using BattleCity.AI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Network.Events;
using BattleCity.StageElements;
using BattleCity.Utils;

namespace BattleCity.Entity
{
    public class Enemy : MultiImageSprite
    {
        enum EnemyStates { Start, Random, PathFinding, BaseDestruction, CollisionWithPlayer, Freezed }

        EnemyStates state;
        EnemyStates previousState;

        Random r = new Random();

        Color originalColor;

        /// <summary>
        /// Ha az állapot random, akkor ebben tároljuk el az előző cella poziciót.
        /// </summary>

        int numberOfShootsToKill;

        /// <summary>
        /// Az előző pixel pozició, ütközéshez.
        /// </summary>
        Vector2 previousPixelPosition;

        float pathFindingTime;
        float randomTime;
        float elapsedGameTimeForStateHandling = 0;
        
        float elapsedGameTimeForFreeze = 0;
        float freezeTimeOut = 3;

        float elapsedGameTimeForPowerUpAnimation = 0;
        float powerUpTimeOut = 5;
        float elapsedGameTimeForPowerUpTimeOut = 0;

		float elapsedGameTimeToTarget = 0;
		float targetingTimeout = 2.0f;
		float elapsedGameTimeToFire = 0;
		float rateOfFire = 4.0f;
		int targetingRange = 50;
		float chancheOfFire = 0.5f;

        public Bullet Bullet
        {
            get { return bullet; }
        }

        public enum EnemyType { EASY, MEDIUM, HARD }

        EnemyType type;
        float speed;
        float bulletSpeed;
        Bullet bullet;
        public event EventHandler Shoot;
        int bulletImageIdx;
        int enemyIdx;

        public int EnemyIdx
        {
            get { return enemyIdx; }
        }

        List<Enemy> enemies;

        Directions direction;

        float remainingPixelsToMove = 50;
        static readonly float pixelsToMove = 50;

        Stage stage;
        int stageIdx;

        Cell previousCell;
        Cell cellPosition;
        Cell nextCell;

        bool dropPowerUp;

        public bool DropPowerUp
        {
            get { return dropPowerUp; }
            set { dropPowerUp = value; }
        }

        public Cell CellPosition
        {
            get { return cellPosition; }
        }

        public Cell NextCellPosition
        {
            get
            {
                if (nextCell != null)
                {
                    return nextCell;
                }
                return null;
            }
        }

        public PowerUp.PTypes PowerUpType
        {
            get;
            set;
        }

        public Enemy(Cell cellPosition, EnemyType type)
            : base(Helper.GetTankAnimatedSpriteImageArray(), Vector2.Zero)
        {
            this.direction = Directions.UP;
            this.type = type;
            this.bulletSpeed = Settings.BulletSpeed;
            this.stage = Controller.Instance.CurrentStage;
            this.stageIdx = Controller.Instance.StageIndex;
            this.cellPosition = cellPosition;
            this.enemies = Controller.Instance.Enemies;
            this.enemyIdx = enemies.Count;
            this.enemies.Add(this);

            cellPosition.IsWall = true;

            state = EnemyStates.Start;

			float hardnessFactor = (0.75f*(float)(stageIdx+1))/10.0f + (0.25f*(float)(enemyIdx+1))/20.0f;
			switch (type) {
				case Enemy.EnemyType.HARD:
					if (Controller.Instance.IsSingle) hardnessFactor *= 1.0f;
					else hardnessFactor *= 1.25f;
					break;
				case Enemy.EnemyType.MEDIUM:
					if (Controller.Instance.IsSingle) hardnessFactor *= 0.75f;
					else hardnessFactor *= 1.0f;
					break;
				default:
					if (Controller.Instance.IsSingle) hardnessFactor *= 0.5f;
					else hardnessFactor *= 0.75f;
					break;
			}

			pathFindingTime = Helper.CalculateEnemyPathFindingTime(hardnessFactor); 
			randomTime = Helper.CalculateEnemyRandomTime(hardnessFactor); 
			targetingRange =  (int)Math.Min(50, 5+Math.Round(50.0f * hardnessFactor));
			chancheOfFire = (float)Math.Min(1.0f, 0.25f + hardnessFactor);
			targetingTimeout = (float)Math.Max (0.5f, targetingTimeout * (1.0f-hardnessFactor));
			rateOfFire = (float)Math.Max (1.0f, rateOfFire * (1.0f-hardnessFactor));

			Console.WriteLine (
				"Enemy[" + enemyIdx + "] x" + hardnessFactor +
				" ("+(targetingRange*13)+"px @ <" +  Math.Round(chancheOfFire*100)+"% / "+targetingTimeout+"sec>) "+ rateOfFire + "sec "+
				"E=<"+ pathFindingTime + "s / " + randomTime + "s>");

            dropPowerUp = r.Next(2) == 1 ? true : false;

            PowerUpType = (PowerUp.PTypes)Enum.Parse(typeof(PowerUp.PTypes), new Random().Next(5).ToString());

            this.CollidesWith(stage.Player1);

            if (!Controller.Instance.IsSingle)
            {
                this.CollidesWith(stage.Player2);
            }

            nextCell = cellPosition;

            position = Helper.CalculateEnemyPixelPosFromCellPos(cellPosition);

            previousPixelPosition = position;

            SetType();
        }

        private void SetType()
        {
            switch (type)
            {
                case EnemyType.EASY:
                    color = Settings.EasyEnemyColor;
                    originalColor = color;
                    speed = Settings.InitialPlayerSpeed;
                    numberOfShootsToKill = 1;
                    break;
                case EnemyType.MEDIUM:
                    color = Settings.MediumEnemyColor;
                    originalColor = color;
                    speed = Settings.InitialPlayerSpeed - 5;
                    numberOfShootsToKill = 2;
                    powerUpTimeOut *= 2;
                    break;
                case EnemyType.HARD:
                    color = Settings.HardEnemyColor;
                    originalColor = color;
                    speed = Settings.InitialPlayerSpeed - 10;
                    numberOfShootsToKill = 3;
                    powerUpTimeOut *= 3;
                    break;
            }
        }

        public void UpdateMovingFromServer(NetworkEnemyMovingEvent e)
        {

            position = e.position;
            direction = e.direciton;
            SetImagesAccordingToDirection(e.moving);
        }


        public override void Update(GameTime gameTime)
        {
            if (state == EnemyStates.Freezed)
            {
                HandleFreezedState(gameTime);
            }
            else
            {
                elapsedGameTimeForStateHandling += (float)gameTime.ElapsedGameTime.TotalSeconds;
				elapsedGameTimeToTarget += (float)gameTime.ElapsedGameTime.TotalSeconds;
				elapsedGameTimeToFire += (float)gameTime.ElapsedGameTime.TotalSeconds;

				if (IsPlayerFrontOfMe() || IsBrickWallFrontOfMeIn(direction))
                {
                    TryToShoot();
                }

                if (state == EnemyStates.Start)
                {
                    TrySwitchState(EnemyStates.PathFinding);
                }
                else if (state == EnemyStates.Random)
                {
                    HandleRandomState(gameTime);
                }
                else if (state == EnemyStates.PathFinding)
                {
                    HandlePathFindingState(gameTime);
                }
                else if (state == EnemyStates.BaseDestruction)
                {
                    HandleBaseDestructionState(gameTime);
                }
                else if (state == EnemyStates.CollisionWithPlayer)
                {
                    HandleCollisionWithPlayerState(gameTime);
                }

                base.Update(gameTime);
            }
        }

        public void UpdateBullet(GameTime gameTime)
        {
            if (bullet != null)
            {
                bullet.Update(gameTime);

                if (bullet != null && bullet.ExplosionEnded)
                {
                    bullet = null;
                }
            }
        }

        public void DropPowerUpUpdate(GameTime gameTime)
        {
            if (dropPowerUp)
            {

                if (elapsedGameTimeForPowerUpTimeOut <= powerUpTimeOut)
                {
                    elapsedGameTimeForPowerUpAnimation += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    elapsedGameTimeForPowerUpTimeOut += (float)gameTime.ElapsedGameTime.TotalSeconds;

                    if (elapsedGameTimeForPowerUpAnimation > 0.075)
                    {
                        if (color == originalColor)
                        {
                            color = Color.White;
                        }
                        else
                        {
                            color = originalColor;
                        }

                        elapsedGameTimeForPowerUpAnimation = 0;
                    }
                }
                else
                {
                    dropPowerUp = false;
                    color = originalColor;
                }
            }
        }

        private void HandleFreezedState(GameTime gameTime)
        {
            elapsedGameTimeForFreeze += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (elapsedGameTimeForFreeze >= freezeTimeOut)
            {
                TrySwitchState(previousState);
                elapsedGameTimeForFreeze = 0;
            }
        }

        private void ChooseNextCell()
        {
            nextCell = CalculatePath();
            if (nextCell == null)
            {
                TrySwitchState(EnemyStates.Random);
            }
        }

        private void TrySwitchState(EnemyStates to)
        {

            switch(to) 
            {
                case EnemyStates.PathFinding:
                    nextCell = CalculatePath();

                    if (nextCell != null)
                    {
                        SwitchState(to);
                    }
                    else
                    {
                        TrySwitchState(EnemyStates.Random);
                    }
                    break;

                case EnemyStates.BaseDestruction:
                    SwitchState(to);
                    break;

                case EnemyStates.Random:

                    ChooseRandomNextCell();

                    if (nextCell != null)
                    {
                        SwitchState(to);
                    }
                    break;

                default:
                    SwitchState(to);
                    break;
            }


        }

        void SwitchState(EnemyStates to)
        {
            if (Settings.TESZT)
            {
                /*Console.WriteLine("Enemy " + enemyIdx + " (" + cellPosition.ToString() + ") switched to " + to.ToString() + " from state " +
                    state.ToString());*/
            }

            elapsedGameTimeForStateHandling = 0;
            previousState = state;
            state = to;
        }

        private void HandleCollisionWithPlayerState(GameTime gameTime)
        {
            if (IsMovingToNextCell)
            {
                MoveToNextCell(gameTime);
            }
            else
            {
                TrySwitchState(previousState);
            }
        }

        private void HandleBaseDestructionState(GameTime gameTime)
        {
            try
            {
                bool shoot = false;
                if (cellPosition.X == stage.EagleCellPosition.X) // egy sorban a sassal
                {
                    if (cellPosition.Y < stage.EagleCellPosition.Y && !IsEnemyFrontOfMeIn(Directions.RIGHT)) // a sastol balra
                    {
                        SetShootingParameters(Directions.RIGHT, out shoot);
                    }
                    else if (!IsEnemyFrontOfMeIn(Directions.RIGHT)) // a sastol jobbra
                    {
                        SetShootingParameters(Directions.LEFT, out shoot);
                    }
                }
                else if (cellPosition.Y == stage.EagleCellPosition.Y && !IsEnemyFrontOfMeIn(Directions.DOWN))
                {
                    SetShootingParameters(Directions.DOWN, out shoot);
                }
                else
                {

                    if (stage.EagleCellPosition.Y < cellPosition.Y)
                    {
                        if (IsBrickWallFrontOfMeIn(Directions.LEFT))
                        {
                            SetShootingParameters(Directions.LEFT, out shoot);
                        }
                        else if (IsBrickWallFrontOfMeIn(Directions.DOWN))
                        {
                            SetShootingParameters(Directions.DOWN, out shoot);
                        }
                    }
                    else if (stage.EagleCellPosition.Y > cellPosition.Y)
                    {
                        if (IsBrickWallFrontOfMeIn(Directions.RIGHT))
                        {
                            SetShootingParameters(Directions.RIGHT, out shoot);
                        }
                        else if (IsBrickWallFrontOfMeIn(Directions.DOWN))
                        {
                            SetShootingParameters(Directions.DOWN, out shoot);
                        }
                    }
                }


                if (shoot)
                {
                    TryToShoot();
                }
                else
                {
                    TrySwitchState(EnemyStates.Random);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Megvizsgálja, hogy a megadott irányban egy cellányira van-e ellenfél.
        /// </summary>
        /// <param name="directions">Az irány, amely felé vizsgálni kell.</param>
        /// <returns>True, ha van előtte ellenfél, false ha nincs.</returns>
        private bool IsEnemyFrontOfMeIn(Directions direction)
        {
            Cell cellToCheck = null;

            switch (direction)
            {
                case Directions.LEFT:
                    cellToCheck = new Cell(cellPosition.X, cellPosition.Y - 1);
                    break;
                case Directions.RIGHT:
                    cellToCheck = new Cell(cellPosition.X, cellPosition.Y + 1);
                    break;
                case Directions.DOWN:
                    cellToCheck = new Cell(cellPosition.X + 1, cellPosition.Y);
                    break;
                case Directions.UP:
                    cellToCheck = new Cell(cellPosition.X - 1, cellPosition.Y);
                    break;
            }

            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemyIdx != i)
                {
                    if (enemies.ElementAt(i).cellPosition.Equals(cellToCheck) ||
                        (enemies.ElementAt(i).NextCellPosition != null && 
                        enemies.ElementAt(i).NextCellPosition.Equals(cellToCheck)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Megvizsgálja, hogy a megadott irányban egy cellányira van-e téglafal.
        /// </summary>
        /// <param name="directions">Az irány, amely felé vizsgálni kell.</param>
        /// <returns>True, ha van előtte ellenfél, false ha nincs.</returns>
        private bool IsBrickWallFrontOfMeIn(Directions direction)
        {
            Vector2 cellToCheck = Vector2.Zero;

            switch (direction)
            {
                case Directions.LEFT:
                    cellToCheck = new Vector2(cellPosition.X, cellPosition.Y - 1);
                    break;
                case Directions.RIGHT:
                    cellToCheck = new Vector2(cellPosition.X, cellPosition.Y + 1);
                    break;
                case Directions.DOWN:
                    cellToCheck = new Vector2(cellPosition.X + 1, cellPosition.Y);
                    break;
                case Directions.UP:
                    cellToCheck = new Vector2(cellPosition.X - 1, cellPosition.Y);
                    break;
            }

            try
            {
                return stage.StageElements[(int)cellToCheck.X, (int)cellToCheck.Y] is BrickWall;
            }
            catch (Exception)
            {

            }
            return false;
        }

        private void HandleRandomState(GameTime gameTime)
        {
            if (!IsMovingToNextCell)
            {
                if (IsBaseNear())
                {
                    TrySwitchState(EnemyStates.BaseDestruction);
                }
                else if (elapsedGameTimeForStateHandling >= randomTime)
                {
					TrySwitchState(EnemyStates.PathFinding);
                }
                else
                {
                    ChooseRandomNextCell();
                }
            }
            else 
            {
                MoveToNextCell(gameTime);
            }
        }

        private void HandlePathFindingState(GameTime gameTime)
        {
            if (!IsMovingToNextCell)
            {
                if (IsBaseNear())
                {
                    TrySwitchState(EnemyStates.BaseDestruction);
                }
                else if (elapsedGameTimeForStateHandling >= pathFindingTime)
                {
                    TrySwitchState(EnemyStates.Random);
                }
                else
                {
                    ChooseNextCell();
                }
            }

            if (IsMovingToNextCell)
            {
                MoveToNextCell(gameTime);
            }
        }

        private bool IsMovingToNextCell
        {
            get { return nextCell != null; }
        }


        /// <summary>
        /// Azért van külön ez, mert ha ütközik a playerrel, akkor még a movingtonextcell az igaz
        /// de a mozgás animációt nem szabad mutatni. Multiplayerben a kliensnek ezt küldjük el mozgásként
        /// </summary>
        public bool IsMovingAnimationOn
        {
            get { return ImageIdx < 4; }
        }

        private bool IsBaseNear()
        {
            foreach (var cellPos in Helper.PossibleEagleFoundCellPositions)
            {
                if (cellPos.Equals(cellPosition))
                {
                    return true;
                }
            }

            return false;
        }

        private void TryToShoot()
        {
			if (bullet == null && elapsedGameTimeToFire > rateOfFire)
            {
				elapsedGameTimeToFire = 0;
                bullet = new Bullet(Resources.BulletImages[bulletImageIdx], bulletSpeed, direction, BulletPosition);

                if (Controller.Instance.IsServer)
                {
                    UpdateShootingToClient();
                }

                if (Shoot != null)
                {
                    Shoot(this, null);
                }

                bullet.CollisionWith += bullet_CollisionWith;
            }
        }

        private void UpdateShootingToClient()
        {
            Controller.Instance.SendNetworkEvent(new NetworkEnemyShootingEvent() { id = enemyIdx });
        }

        private void ChooseRandomNextCell()
        {
            var blockingGrid = StageToNodeGrid();

            int x = cellPosition.X;
            int y = cellPosition.Y;

            bool up = false;
            bool down = false;
            bool left = false;
            bool right = false;


            while ((!up || !down || !left || !right) && nextCell == null)
            {
                switch (r.Next(4))
                {
                    case 0:
                        if (y - 1 >= 0 && !blockingGrid[x, y - 1].IsWall)
                        {
                            if (NotAPreviusCell(blockingGrid[x, y - 1]) || (up && down && left))
                            {
                                nextCell = blockingGrid[x, y - 1];
                            }
                        }
                        right = true;
                        break;
                    case 1:
                        if (y + 1 < blockingGrid.GetLength(1) && !blockingGrid[x, y + 1].IsWall)
                        {
                            if (NotAPreviusCell(blockingGrid[x, y + 1]) || (right && up && down))
                            {
                                nextCell = blockingGrid[x, y + 1];
                            }
                        }
                        left = true;
                        break;
                    case 2:
                        if (x - 1 >= 0 && !blockingGrid[x - 1, y].IsWall)
                        {
                            if (NotAPreviusCell(blockingGrid[x - 1, y]) || (down && left && right))
                            {
                                nextCell = blockingGrid[x - 1, y];
                            }

                        }
                        up = true;
                        break;
                    case 3:
                        if (x + 1 < blockingGrid.GetLength(0) && !blockingGrid[x + 1, y].IsWall)
                        {
                            if (NotAPreviusCell(blockingGrid[x + 1, y]) || (up && left && right))
                            {
                                nextCell = blockingGrid[x + 1, y];
                            }
                        }
                        down = true;
                        break;
                }
            }

            if (nextCell != null)
            {
                nextCell.IsWall = true;
            }

        }

        private bool IsPlayerFrontOfMe(bool checkHide = true)
        {
			// megadott időközönként próbál célpontot keresni
			if (elapsedGameTimeToTarget < targetingTimeout) {
				return false;
			}
			elapsedGameTimeToTarget = 0;

			// megadott eséllyel kezd bele a célkeresésbe az adott ciklusban
			float rnd = ((float)Helper.GetRandomNumber (1, 100)) / 100.0f;
			if (rnd > chancheOfFire) {
				return false;
			}

            var blockingGrid = StageToNodeGrid();
            int widthOrHeight = 0;

            Rectangle collider = new Rectangle();

            switch (direction)
            {
                case Directions.UP:
                    for (int i = (int)cellPosition.X - 1; i >= 0; i--)
                    {
						if (!blockingGrid[i, (int)cellPosition.Y].IsWall)
                        {
                            widthOrHeight += this.targetingRange;
                        }
                        else
                        {
                            break;
                        }
                    }
                    collider = new Rectangle((int)position.X + (int)Settings.BulletPositionUp.X,
                        (int)position.Y + (int)Settings.BulletPositionUp.Y - widthOrHeight, 
                        (int)Resources.BulletImages[0].GetCurrentImageSize().X, widthOrHeight);
                    break;
                case Directions.DOWN:
                    for (int i = (int)cellPosition.X + 1; i < blockingGrid.GetLength(0); i++)
                    {
						if (!blockingGrid[i, (int)cellPosition.Y].IsWall)
                        {
							widthOrHeight += this.targetingRange;
                        }
                        else
                        {
                            break;
                        }
                    }
                    collider = new Rectangle((int)position.X + (int)Settings.BulletPositionDown.X,
                        (int)position.Y + (int)Settings.BulletPositionDown.Y,
                        (int)Resources.BulletImages[1].GetCurrentImageSize().X, widthOrHeight);
                    break;
                case Directions.LEFT:
                    for (int j = (int)cellPosition.Y - 1; j >= 0; j--)
                    {
						if (!blockingGrid[(int)cellPosition.X, j].IsWall)
                        {
							widthOrHeight += this.targetingRange;
                        }
                        else
                        {
                            break;
                        }
                    }
                    collider = new Rectangle((int)position.X + (int)Settings.BulletPositionLeft.X - widthOrHeight,
                        (int)position.Y + (int)Settings.BulletPositionLeft.Y,
                        widthOrHeight, (int)Resources.BulletImages[2].GetCurrentImageSize().Y);
                    break;
                case Directions.RIGHT:
                    for (int j = (int)cellPosition.Y + 1; j < blockingGrid.GetLength(1); j++)
                    {
						if (!blockingGrid[(int)cellPosition.X, j].IsWall)
                        {
							widthOrHeight += this.targetingRange;
                        }
                        else
                        {
                            break;
                        }
                    }
                    collider = new Rectangle((int)position.X + (int)Settings.BulletPositionRight.X,
                        (int)position.Y + (int)Settings.BulletPositionRight.Y,
                        widthOrHeight, (int)Resources.BulletImages[3].GetCurrentImageSize().Y);
                    break;
            }

            bool r = collider.Intersects(stage.Player1.GetCollider());

            if (checkHide)
            {
                r = r && !stage.Player1.IsHide;
            }

            if (!r && !Controller.Instance.IsSingle)
            {
                r = collider.Intersects(stage.Player2.GetCollider());

                if (checkHide)
                {
                    r = r && !stage.Player2.IsHide;
                }
            }

            return r;
        }

        private bool NotAPreviusCell(Cell possibleNextCell)
        {
            if (previousCell != null)
            {
                if (previousCell.Equals(possibleNextCell))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);

            if (Settings.TESZT)
            {
                batch.DrawString(Resources.Font, enemyIdx.ToString(), position, Color.White);
            }
        }

        public void MoveToNextCell(GameTime gameTime)
        {
            float deltaT = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float movement = speed * deltaT;
            
            previousPixelPosition = position;

            if (cellPosition.X == nextCell.X)
            {
                if (nextCell.Y > cellPosition.Y)
                {
                    direction = Directions.RIGHT;
                }
                else
                {
                    direction = Directions.LEFT;
                }
            }
            else
            {
                if (nextCell.X > cellPosition.X)
                {
                    direction = Directions.DOWN;
                }
                else
                {
                    direction = Directions.UP;
                }
            }

            remainingPixelsToMove -= movement;

            if (remainingPixelsToMove < 0)
            {
                movement += remainingPixelsToMove;
            }

            SetImagesAccordingToDirection(IsMovingToNextCell);

            switch (direction)
            {
                case Directions.UP:
                    position.Y -= movement;
                    break;
                case Directions.DOWN:
                    position.Y += movement;
                    break;
                case Directions.LEFT:
                    position.X -= movement;
                    break;
                case Directions.RIGHT:
                    position.X += movement;
                    break;
            }

            if (remainingPixelsToMove <= 0)
            {
                previousCell = cellPosition;
                cellPosition = nextCell;
                this.nextCell = null;
                remainingPixelsToMove = pixelsToMove;
            }
        }

        private void SetImagesAccordingToDirection(bool isMoving)
        {
            int baseIdx = 4;

            if (isMoving)
            {
                baseIdx = 0;
            }

            switch (direction)
            {
                case Directions.UP:
                    SetCurrentImage(baseIdx);
                    bulletImageIdx = 0;
                    break;
                case Directions.DOWN:
                    SetCurrentImage(baseIdx + 1);
                    bulletImageIdx = 1;
                    break;
                case Directions.LEFT:
                    SetCurrentImage(baseIdx + 2);
                    bulletImageIdx = 2;
                    break;
                case Directions.RIGHT:
                    SetCurrentImage(baseIdx + 3);
                    bulletImageIdx = 3;
                    break;
            }
        }

        private Cell[,] StageToNodeGrid()
        {
            var stageArray = stage.StageElements;
            var cellArray = new Cell[stageArray.GetLength(0), stageArray.GetLength(1)];

            for (int i = 0; i < stageArray.GetLength(0); i++)
            {
                for (int j = 0; j < stageArray.GetLength(1); j++)
                {
                    if (stageArray[i, j] == null || (stageArray[i, j] != null && (stageArray[i, j] is Icefield
                        || stageArray[i, j] is Bush)))
                    {
                        cellArray[i, j] = new Cell(i, j);
                    }
                    else
                    {
                        cellArray[i, j] = new Cell(i, j, true);
                    }
                }
            }

            for (int i = 0; i < enemies.Count; i++)
            {
                var enemy = enemies.ElementAt(i);
                if (i != enemyIdx && !enemy.IsDestroyed())
                {
                    Cell c = enemy.cellPosition;
                    Cell cNext = enemy.NextCellPosition;

                    cellArray[c.X, c.Y] = new Cell(c.X, c.Y, true);

                    if (cNext != null)
                    {
                        cellArray[cNext.X, cNext.Y] = new Cell(cNext.X, cNext.Y, true);
                    }
                }
            }

            return cellArray;
        }

        private Cell CalculatePath()
        {
            var grid = StageToNodeGrid();

            var pathFinder = new SpatialAStar<Cell, Object>(grid);
            LinkedList<Cell> path = pathFinder.Search(cellPosition, Helper.AStarPossibleEndPositions.ElementAt(0), null);

            int i = 1;
            while (path == null && i < Helper.AStarPossibleEndPositions.Count)
            {
                path = pathFinder.Search(cellPosition, Helper.AStarPossibleEndPositions.ElementAt(i), null);
                i++;
            }

            if (path != null && path.Count != 1)
            {
                path.ElementAt(1).IsWall = true;
                return path.ElementAt(1);
            }
            else
            {
                return null;
            }
        }

        private Vector2 BulletPosition
        {
            get
            {
                Vector2 bulletPos = position;

                switch (direction)
                {
                    case Directions.UP:
                        bulletPos += Settings.BulletPositionUp;
                        break;
                    case Directions.DOWN:
                        bulletPos += Settings.BulletPositionDown;
                        break;
                    case Directions.LEFT:
                        bulletPos += Settings.BulletPositionLeft;
                        break;
                    case Directions.RIGHT:
                        bulletPos += Settings.BulletPositionRight;
                        break;
                }

                return bulletPos;
            }
        }

        void bullet_CollisionWith(BasicSprite other)
        {
            if (other is Enemy)
            {
                ((Enemy)other).TryToKillMe();
            }
            else if (other is Player)
            {
                ((Player)other).TryToKillMe();
            }
            else if (other is SteelWall && type == EnemyType.HARD)
            {
                var bw = (SteelWall)other;
                bw.DecrementShotsCount(1);
            }

            if (Controller.Instance.IsServer)
            {
                UpdateBulletCollideToClient();
            }

        }

        public override void OnCollisionWith(BasicSprite other)
        {
            if (other is Player)
            {
                remainingPixelsToMove += Math.Abs(position.X - previousPixelPosition.X);
                remainingPixelsToMove += Math.Abs(position.Y - previousPixelPosition.Y);

                position = previousPixelPosition;

                SetImagesAccordingToDirection(false);

                if (state != EnemyStates.CollisionWithPlayer)
                {
                    TrySwitchState(EnemyStates.CollisionWithPlayer);
                }
            }
        }

        private void SetShootingParameters(Directions direction, out bool shoot)
        {
            shoot = true;
            this.direction = direction;
            switch (direction)
            {
                case Directions.LEFT:
                    bulletImageIdx = 2;
                    SetCurrentImage(6);
                    break;
                case Directions.RIGHT:
                    bulletImageIdx = 3;
                    SetCurrentImage(7);
                    break;
                case Directions.DOWN:
                    SetCurrentImage(5);
                    bulletImageIdx = 1;
                    break;
                case Directions.UP:
                    bulletImageIdx = 0;
                    SetCurrentImage(4);
                    break;
            }

        }


        public void TryToKillMe()
        {
            numberOfShootsToKill--;
            if (numberOfShootsToKill == 0)
            {
                Destroy();
            }

            if (!Controller.Instance.IsSingle)
            {
                Controller.Instance.SendNetworkEvent(new NetworkEnemyDeadEvent() { id = enemyIdx });
            }
        }

        public void UpdateDamagedFromPartner()
        {
            numberOfShootsToKill--;
            if (numberOfShootsToKill == 0)
            {
                Destroy();
            }
        }

        public void Freeze()
        {
            TrySwitchState(EnemyStates.Freezed);
            SetImagesAccordingToDirection(false);
        }

        public bool IsFreezed 
        {
            get { return state == EnemyStates.Freezed; }
        }

        public bool Firing
        {
            get { return bullet != null; }
        }

        internal void DrawBullet(SpriteBatch spriteBatch)
        {
            if (bullet != null)
            {
                bullet.Draw(spriteBatch);
            }
        }

        public Directions Direction
        {
            get { return direction; }
        }


        public void UpdateBulletCollideFromServer()
        {
			if (bullet != null) {
				bullet.Explode ();
			} else {
				Console.WriteLine ("FAIL: Enemy.UpdateBulletCollideFromServer");
			}
        }

        public void UpdateShootingFromServer()
        {
            bullet = new Bullet(Resources.BulletImages[bulletImageIdx], bulletSpeed, direction, BulletPosition);
        }

        public void UpdateBulletCollideToClient()
        {
            Controller.Instance.SendNetworkEvent(new NetworkEnemyBulletCollideEvent() { id = enemyIdx });
        }
    }
}
