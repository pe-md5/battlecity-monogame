﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BattleCity.GUI
{
    public class Button : MultiImageSprite
    {
        string text;
        SpriteFont font;
        Vector2 textPosition;

        bool pressed = false;

        public event EventHandler Click;

        public Button(SimpleSpriteImage[] images, Vector2 position, string text = "", SpriteFont font = null)
            : base(images, position)
        {
            this.text = text;
            this.font = font;

            Vector2 imageSize = images[0].GetCurrentImageSize();
            if (font != null)
            {
                Vector2 textSize = font.MeasureString(text);
                textPosition.X = position.X + (imageSize.X - textSize.X) / 2;
                textPosition.Y = position.Y + (imageSize.Y - textSize.Y) / 2;
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            if (font != null)
            {
                batch.DrawString(font, text, textPosition, Color.White);
            }
        }

        public override void Update(GameTime gameTime)
        {

			var mouseState = Microsoft.Xna.Framework.Input.Mouse.GetState();
			if (mouseState.X >= position.X && mouseState.Y >= position.Y &&
			    mouseState.X <= position.X + this.GetSpriteImage().GetCurrentImageSize().X &&
			    mouseState.Y <= position.Y + this.GetSpriteImage().GetCurrentImageSize().Y)
			{
                SetCurrentImage(1);
                
                if (Mouse.GetState().LeftButton == ButtonState.Released && pressed)
                {
                    if (Click != null)
                    {
                        Click(this, null);
                    }
                }

            }
            else
            {
                SetCurrentImage(0);
            }

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                pressed = true;
            }
            else
            {
                pressed = false;
            }

            base.Update(gameTime);
        }
    }
}
