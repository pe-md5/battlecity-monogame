﻿using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Utils;

namespace BattleCity.GUI
{
    public class List
    {
        Vector2 position;
        SpriteFont font;
        
        ListItem selectedItem;
        List<ListItem> itemList;

        int startIndex;

        Button prevButton;
        Button nextButton;

        BasicSprite background;

        public ListItem SelectedItem
        {
            get { return selectedItem; }
        }

        public List(Vector2 position, SpriteFont font)
        {
            this.position = position;
            this.font = font;
            this.startIndex = 0;

            var buttonImageSize = Resources.PrevButtonImages[0].GetCurrentImageSize();
            var backgroundImageSize = Resources.ListBackgroundImage.GetCurrentImageSize();
            var prevButtonPos = new Vector2(position.X, position.Y + (backgroundImageSize.Y - buttonImageSize.Y) / 2);
            prevButton = new Button(Resources.PrevButtonImages, prevButtonPos);
            prevButton.Click += button_Click;

            var backgroundPos = new Vector2(prevButtonPos.X + 10 + prevButton.GetSize().X, position.Y);
            background = new BasicSprite(Resources.ListBackgroundImage, backgroundPos);

            var nextButtonPos = new Vector2(backgroundPos.X + 10 + background.GetSize().X, prevButtonPos.Y);
            nextButton = new Button(Resources.NextButtonImages, nextButtonPos);
            nextButton.Click += button_Click;


            itemList = new List<ListItem>();
        }

        void button_Click(object sender, EventArgs e)
        {
            if (sender == nextButton)
            {
                if (startIndex + 5 < itemList.Count)
                {
                    startIndex += 5;
                }
            }
            else
            {
                startIndex -= 5;

                if (startIndex < 0)
                {
                    startIndex = 0;
                }
            }
        }

        public void Add(string text)
        {
            if (itemList.Count % 5 == 0)
            {
                var item = new ListItem(new Vector2(background.Position.X + 15, background.Position.Y + 15), Resources.ListItemImages, text, font);
                itemList.Add(item);
                item.Selected += item_Selected;
            }
            else
            {
                var pos = itemList.ElementAt(itemList.Count - 1).Position;
                var size = itemList.ElementAt(itemList.Count - 1).GetSize();
                var item = new ListItem(new Vector2(pos.X, pos.Y + 2 + size.Y), Resources.ListItemImages, text, font);
                item.Selected += item_Selected;
                itemList.Add(item);
            }
        }

        public static Vector2 GetSize()
        {
            return new Vector2(Resources.PrevButtonImages[0].GetCurrentImageSize().X * 2 + 30 +
                Resources.ListBackgroundImage.GetCurrentImageSize().X,
                Resources.ListBackgroundImage.GetCurrentImageSize().Y);
        }

        public void InitializeItemList(string[] itemListTexts)
        {
            itemList = new List<ListItem>();

            for(int i = 0; i < itemListTexts.Length; i++)
            {
                string text = itemListTexts.ElementAt(i);
                if (i % 5 == 0)
                {
                    var item = new ListItem(new Vector2(background.Position.X + 15, background.Position.Y + 15), Resources.ListItemImages, text, font);
                    itemList.Add(item);
                    item.Selected += item_Selected;
                }
                else
                {
                    var pos = itemList.ElementAt(i - 1).Position;
                    var size = itemList.ElementAt(i - 1).GetSize();
                    var item = new ListItem(new Vector2(pos.X, pos.Y + 2 + size.Y), Resources.ListItemImages, text, font);
                    item.Selected += item_Selected;
                    itemList.Add(item);
                }
            }
        }

        void item_Selected(object sender, EventArgs e)
        {
            selectedItem = sender as ListItem;
            for (int i = startIndex; i < startIndex + 5 && i < itemList.Count; i++)
            {
                itemList.ElementAt(i).RemoveSelection();
            }
        }

        public void Draw(SpriteBatch batch)
        {
            background.Draw(batch);
            nextButton.Draw(batch);
            prevButton.Draw(batch);
            

            for (int i = startIndex; i < startIndex + 5 && i < itemList.Count; i++)
            {
                itemList.ElementAt(i).Draw(batch);
            }

           
            string pageNumber = "";

            if (startIndex == 0)
            {
                pageNumber += "1/";
            }
            else
            {
                pageNumber += (startIndex / 5 + ((startIndex % 5 == 0) ? 1 : 0)).ToString() + "/";
            }

            if (itemList.Count == 0)
            {
                pageNumber += "1";
            }
            else
            {
                pageNumber += (itemList.Count / 5 + ((itemList.Count % 5 == 0) ? 0 : 1)).ToString();
            }

            var strSize = font.MeasureString(pageNumber);
            var bgsize = background.GetSize();
            var bgposition = background.Position;
            var strPosition = new Vector2((bgsize.X - strSize.X) / 2 + bgposition.X,  bgposition.Y + bgsize.Y + 10);

            batch.DrawString(font, pageNumber, strPosition, Color.White);
        }


        public void Update(GameTime gameTime)
        {
            nextButton.Update(gameTime);
            prevButton.Update(gameTime);

            for (int i = startIndex; i < startIndex + 5 && i < itemList.Count; i++)
            {
                itemList.ElementAt(i).Update(gameTime);
            }
        }


        public void Clear()
        {
            this.startIndex = 0;
            itemList.Clear();
            selectedItem = null;
        }
    }
}
