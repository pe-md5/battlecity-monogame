﻿using BattleCity.Entity;
using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.StageElements;
using BattleCity.Utils;


namespace BattleCity.GUI
{
    public class MainMenu
    {
        BasicSprite titleSprite;
        Button spButton;
        Button mpButton;
        Button exitButton;

        static Stage s = new Stage(Controller.Instance, StageDescriptions.Get(0), null, null, true);

        public static Stage MenuStageBackground 
        {
            get { return s; }
        }

        public MainMenu(Rectangle size)
        {
            int titlePosX = size.X + (size.Width - (int)Resources.TitleImage.GetCurrentImageSize().X)/2;
            titleSprite = new BasicSprite(Resources.TitleImage, new Vector2(titlePosX, 50));

            Vector2 button1Pos = new Vector2(size.X + (size.Width - (int)Resources.ButtonImages[0].GetCurrentImageSize().X) / 2, Resources.TitleImage.GetCurrentImageSize().Y + 200);

            spButton = new Button(Resources.ButtonImages, button1Pos, "SINGLEPLAYER", Resources.Font);
            spButton.Click += Button_Click;

            mpButton = new Button(Resources.ButtonImages, button1Pos + new Vector2(0, 60), "MULTIPLAYER", Resources.Font);
            mpButton.Click += Button_Click;

            exitButton = new Button(Resources.ButtonImages, button1Pos + new Vector2(0, 120), "QUIT", Resources.Font);
            exitButton.Click += Button_Click;

            
        }

        void Button_Click(object sender, EventArgs e)
        {
			if (sender == exitButton) {
				Game1.Instance.Exit ();
			} else if (sender == spButton) {
				Game1.Instance.State = GameState.SPLoading;
			} else if (sender == mpButton) {
                Game1.Instance.State = GameState.MPMenu;
			}
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            s.DrawForMenu(spriteBatch);

            titleSprite.Draw(spriteBatch);
            spButton.Draw(spriteBatch);
            mpButton.Draw(spriteBatch);
            exitButton.Draw(spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            spButton.Update(gameTime);
            mpButton.Update(gameTime);
            exitButton.Update(gameTime);
        }
    }
}
