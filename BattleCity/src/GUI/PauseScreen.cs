﻿using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Utils;

namespace BattleCity.GUI
{
    public class PauseScreen
    {
        BasicSprite titleSprite;
        Button continueButton;
        Button quitToMenuButton;
        Button quitFromGameButton;

        public PauseScreen(Rectangle size)
        {

            int titlePosX = size.X + (size.Width - (int)Resources.TitleImage.GetCurrentImageSize().X) / 2;
            titleSprite = new BasicSprite(Resources.TitleImage, new Vector2(titlePosX, 50));

            Vector2 button1Pos = new Vector2(size.X + (size.Width - (int)Resources.ButtonImages[0].GetCurrentImageSize().X) / 2, Resources.TitleImage.GetCurrentImageSize().Y + 200);

            continueButton = new Button(Resources.ButtonImages, button1Pos, "Continue", Resources.Font);
            continueButton.Click += Button_Click;

            quitToMenuButton = new Button(Resources.ButtonImages, button1Pos + new Vector2(0, 60), "Quit to menu", Resources.Font);
            quitToMenuButton.Click += Button_Click;

            quitFromGameButton = new Button(Resources.ButtonImages, button1Pos + new Vector2(0, 120), "Quit", Resources.Font);
            quitFromGameButton.Click += Button_Click;
        }

        void Button_Click(object sender, EventArgs e)
        {
            if (sender == quitFromGameButton)
            {
                Game1.Instance.Exit();
            }
            else if (sender == continueButton)
            {
                if (Game1.Instance.State == GameState.MPPause)
                {
                    Game1.Instance.State = GameState.InMP;
                }
                else
                {
                    Game1.Instance.State = GameState.InSP;
                    Controller.Instance.Resume();
                }
            }
            else if (sender == quitToMenuButton)
            {
                if (Game1.Instance.State == GameState.MPPause)
                {
                    Controller.Instance.StopClient();
                    Controller.Instance.StopServer();
                }
                Controller.Instance.StageIndex = 0;
                Game1.Instance.State = GameState.MainMenu;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            titleSprite.Draw(spriteBatch);
            continueButton.Draw(spriteBatch);
            quitToMenuButton.Draw(spriteBatch);
            quitFromGameButton.Draw(spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            continueButton.Update(gameTime);
            quitToMenuButton.Update(gameTime);
            quitFromGameButton.Update(gameTime);
        }
    }
}
