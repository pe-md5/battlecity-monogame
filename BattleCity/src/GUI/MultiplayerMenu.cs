﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BattleCity.Utils;

namespace BattleCity.GUI
{
    public class MultiplayerMenu {

        BasicSprite titleSprite;
        Button startServer;
        Button discover;
        Button backToMainMenu;

        public MultiplayerMenu(Rectangle size)
        {
            int titlePosX = size.X + (size.Width - (int)Resources.TitleImage.GetCurrentImageSize().X)/2;
            titleSprite = new BasicSprite(Resources.TitleImage, new Vector2(titlePosX, 50));

            Vector2 button1Pos = new Vector2(size.X + (size.Width - (int)Resources.ButtonImages[0].GetCurrentImageSize().X) / 2, Resources.TitleImage.GetCurrentImageSize().Y + 200);

            startServer = new Button(Resources.ButtonImages, button1Pos, "START SERVER", Resources.Font);
            startServer.Click += Button_Click;

            discover = new Button(Resources.ButtonImages, button1Pos + new Vector2(0, 60), "DISCOVER", Resources.Font);
            discover.Click += Button_Click;



            backToMainMenu = new Button(Resources.ButtonImages, button1Pos + new Vector2(0, 120), "BACK", Resources.Font);
            backToMainMenu.Click += Button_Click;
        }

        void Button_Click(object sender, EventArgs e)
        {
			if (sender == backToMainMenu) {
                Game1.Instance.State = GameState.MainMenu;
			} else if (sender == startServer) {
                Game1.Instance.State = GameState.MPServerStart;
			} else if (sender == discover) {
                Controller.Instance.DiscoverServers();
                Game1.Instance.State = GameState.MPClientDiscover;
			}
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            MainMenu.MenuStageBackground.DrawForMenu(spriteBatch);

            titleSprite.Draw(spriteBatch);
            startServer.Draw(spriteBatch);
            discover.Draw(spriteBatch);
            backToMainMenu.Draw(spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            startServer.Update(gameTime);
            discover.Update(gameTime);
            backToMainMenu.Update(gameTime);
        }
    }
}
