﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.GUI
{
    public class LoadingScreen
    {
        string text;
        Vector2 textPos;
        SpriteFont font;
        float delay;
        float elapsedGameTime = 0.0f;

        public event EventHandler Loaded;

        public LoadingScreen(string text, int screenWidth, int screenHeight, SpriteFont font, float delay)
        {
            var textSize = font.MeasureString(text);
            textPos = new Vector2((screenWidth - textSize.X) / 2, (screenHeight - textSize.Y) / 2);
            this.font = font;
            this.text = text;
            this.delay = delay;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, textPos, Color.White);
        }

        public void Update(GameTime gameTime)
        {

            elapsedGameTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
			if ( elapsedGameTime >= delay)
            {
                if (Loaded != null)
                {
                    Loaded(this, null);
                }
            }
        }

    }
}
