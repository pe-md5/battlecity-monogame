﻿using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Utils;

namespace BattleCity.GUI
{
    public class ListItem : MultiImageSprite
    {
        Vector2 textPosition;
        string text;
        SpriteFont font;
        bool selected = false;
        public event EventHandler Selected;

        public ListItem(Vector2 position, SimpleSpriteImage[] images, string text, SpriteFont font)
            :base(images, position)
        {
            this.text = text;
            this.font = font;
            textPosition = Helper.CalculateTextPositionForGUI(this, text, font);
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            batch.DrawString(font, text, textPosition, Color.White);
        }

        public string Text
        {
            get { return text; }
        }

        public override void Update(GameTime gameTime)
        {
            if (!selected)
            {
                var mouseState = Microsoft.Xna.Framework.Input.Mouse.GetState();
                if (mouseState.X >= position.X && mouseState.Y >= position.Y &&
                    mouseState.X <= position.X + this.GetSpriteImage().GetCurrentImageSize().X &&
                    mouseState.Y <= position.Y + this.GetSpriteImage().GetCurrentImageSize().Y)
                {
                    SetCurrentImage(1);

                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        if (Selected != null)
                        {
                            Selected(this, null);
                            selected = true;
                            SetCurrentImage(2);
                        }
                    }

                }
                else
                {
                    SetCurrentImage(0);
                }
            }
        }

        public void RemoveSelection()
        {
            selected = false;
        }

    }
}
