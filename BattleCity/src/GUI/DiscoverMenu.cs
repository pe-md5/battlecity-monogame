﻿using BattleCity.GUI;
using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using BattleCity.Utils;

namespace BattleCity.GUI
{
    public class DiscoverMenu
    {
        List ipList;

        static DiscoverMenu instance;

        Button back; // vissza a főmenübe, cliens megszüntetése, nullázása, valamint itt az iplista üritése
        Button refresh; // discover újra, cliens.discover, iplisat ürítése
        Button connect; // csatlakozás

        BasicSprite title;

        public List IPList
        {
            get { return ipList; }
        }

        public static DiscoverMenu Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DiscoverMenu(new Rectangle(0, 0, Game1.Instance.GraphicsDevice.Viewport.Width, Game1.Instance.GraphicsDevice.Viewport.Height));
                }
                return instance;
            }
        }

        public void ClearIPList()
        {
            ipList.Clear();
        }

        private DiscoverMenu(Rectangle size)
        {

            int titlePosX = size.X + (size.Width - (int)Resources.TitleImage.GetCurrentImageSize().X) / 2;
            title = new BasicSprite(Resources.TitleImage, new Vector2(titlePosX, 50));

            Vector2 ipListPos = new Vector2(size.X + (size.Width - List.GetSize().X) / 2, Resources.TitleImage.GetCurrentImageSize().Y + 100);
            ipList = new List(ipListPos, Resources.Font);

            Vector2 firstButtonPos = new Vector2((size.Width -
                Resources.ButtonImages[0].GetCurrentImageSize().X * 3) / 2,
                ipListPos.Y + List.GetSize().Y + 50);
                ;
            

            connect = new Button(Resources.ButtonImages, firstButtonPos, "CONNECT", Resources.Font);
            connect.Click += Button_Click;

            refresh = new Button(Resources.ButtonImages, firstButtonPos + 
                new Vector2(Resources.ButtonImages[0].GetCurrentImageSize().X, 0), "REFRESH", Resources.Font);
            refresh.Click += Button_Click;

            back = new Button(Resources.ButtonImages, firstButtonPos + 
                new Vector2(Resources.ButtonImages[0].GetCurrentImageSize().X*2, 0), "BACK", Resources.Font);
            back.Click += Button_Click;
        }

        void Button_Click(object sender, EventArgs e)
        {
            if (sender == back)
            {
                Game1.Instance.State = GameState.MPMenu;
                instance = null;
                Controller.Instance.StopClient();
            }
            else if (sender == refresh)
            {
                ipList.Clear();
                Controller.Instance.DiscoverServers();
            }
            else if (sender == connect)
            {
                if (ipList.SelectedItem != null)
                {
                    Game1.Instance.State = GameState.MPLoading;
                }
            }
        }

        public IPAddress SelectedIPAddress
        {
            get { return IPAddress.Parse(ipList.SelectedItem.Text); }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            MainMenu.MenuStageBackground.DrawForMenu(spriteBatch);

            title.Draw(spriteBatch);
            back.Draw(spriteBatch);
            connect.Draw(spriteBatch);
            refresh.Draw(spriteBatch);
            ipList.Draw(spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            ipList.Update(gameTime);
            back.Update(gameTime);
            refresh.Update(gameTime);
            connect.Update(gameTime);
        }
    }
}
