﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;
using BattleCity.Entity;
using BattleCity.Effect;
using System.Threading;
using BattleCity.AI;
using BattleCity.StageElements;
using BattleCity.Utils;

namespace BattleCity
{
    public class Stage
    {
        BasicSprite[,] stageElements;
        BasicSprite background;
        BasicSprite[] borders;
        Player player1;
        Player player2;
        List<Enemy> enemies; // just for drawing logic
        List<PowerUp> powerUps; // just for drawing logic
        Eagle eagle;

        Vector2 livesTextPos = new Vector2(Settings.StageBorderSize + 10, Settings.StageBorderSize);

        Color textColor = new Color(0, 0, 0, 150);

        public BasicSprite[,] StageElements
        {
            get
            {
                return stageElements;
            }
        }

        public bool IsEagleDestroyed
        {
            get { return eagle.IsDestroyed(); }
        }

        /*
 * 
 * Stage description:
 * ; - üres sor
 * BR - tégla 
 * SW - acélfal
 * WA - víz
 * BU - bokor
 * IF - jég
 * E - üres
 * EA - EAGLE
 * P1 - player1 kezdeti pozició
 * P2 - player2 kezdeti pozició
 * Az elemek vesszővel elválasztva
 * Pl.:
 * E, BR, IF, BR, ...
 * 13 db egy sorban végén ;
 * 13 sor
 * 
 * */
        public Stage(Controller controller, string stageDescriptor, List<Enemy> enemies, List<PowerUp> powerUps, bool dummy = false)
        {
            this.enemies = enemies;
            this.powerUps = powerUps;

            background = new BasicSprite(Resources.StageBackground, Settings.StagePosition);

            stageElements = new BasicSprite[13, 13];

            borders = new BasicSprite[4];
            borders[0] = new BasicSprite(Resources.StageBorderHorizontal, Settings.StagePosition);
            borders[1] = new BasicSprite(Resources.StageBorderHorizontal, new Vector2(0, Settings.StagePosition.Y + 13 * Settings.StageElementSize.Y + Settings.StageBorderSize));
            borders[2] = new BasicSprite(Resources.StageBorderVertical, Settings.StagePosition);
            borders[3] = new BasicSprite(Resources.StageBorderVertical, new Vector2(Settings.StagePosition.X + 13 * Settings.StageElementSize.X + Settings.StageBorderSize, 0));

            try
            {
                string[] stageRows = stageDescriptor.Split(';');

                for (int i = 0; i < 13; i++)
                {
                    string[] elements = stageRows[i].Split(',');

                    if (elements.Length > 1)
                    {
                        for (int j = 0; j < 13; j++)
                        {
                            Vector2 position = new Vector2(Settings.StagePosition.X + Settings.StageBorderSize + Settings.StageElementSize.X * j,
                                                Settings.StagePosition.Y + Settings.StageBorderSize + Settings.StageElementSize.Y * i);

                            switch (elements[j])
                            {
                                case "BR":
                                    stageElements[i, j] = new BrickWall(Resources.BrickWallImages, position, new Cell(i, j));
                                    (stageElements[i, j] as BrickWall).DestroyEvent += StageElement_Destroy;
                                    break;
                                case "IF":
                                    stageElements[i, j] = new Icefield(Resources.IcefieldImage, position);
                                    break;
                                case "WA":
                                    stageElements[i, j] = new Water(Resources.WaterImage, position);
                                    break;
                                case "BU":
                                    stageElements[i, j] = new Bush(Resources.BushImage, position);
                                    break;
                                case "SW":
                                    stageElements[i, j] = new SteelWall(Resources.SteelWallImages, position, new Cell(i, j));
                                    (stageElements[i, j] as SteelWall).DestroyEvent += StageElement_Destroy;
                                    break;
                                case "P1":
                                    player1 = new Player(1, Settings.InitialPlayerSpeed, Settings.BulletSpeed, position, Settings.Player1Color);
                                    break;
                                case "P2":
                                    if (!controller.IsSingle)
                                    {
                                        player2 = new Player(2, Settings.InitialPlayerSpeed, Settings.BulletSpeed, position, Settings.Player2Color);
                                    }

                                    break;
                                case "EA":
                                    stageElements[i, j] = new Eagle(Resources.EagleImages, position, new Cell(i, j));
                                    eagle = (Eagle)stageElements[i, j];
                                    EagleCellPosition = new Cell(i, j, true);
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                }

            }
            catch (Exception)
            {
                Console.WriteLine("Wrong stage description: " + stageDescriptor);
            }
        }

        void StageElement_Destroy(object sender, EventArgs e)
        {
            for (int i = 0; i < stageElements.GetLength(0); i++)
            {
                for (int j = 0; j < stageElements.GetLength(1); j++)
                {
                    if (stageElements[i, j] == sender)
                    {
                        stageElements[i, j] = null;
                    }
                }
            }
        }



        public void Update(GameTime gameTime)
        {
			try {

	            foreach (var element in stageElements)
	            {
	                if (element != null)
	                {
	                    element.Update(gameTime);
	                }
	            }

	            if (Controller.Instance.IsSingle || Controller.Instance.IsServer)
	            {

	                if (Controller.Instance.IsServer)
	                {
	                    player2.UpdateBullet(gameTime);
	                    player2.UpdateProtectorField(gameTime);
                        player2.UpdateImage(gameTime);
	                }

                    if (!player1.IsDestroyed()) 
                    { 
	                    player1.Update(gameTime);
	                    player1.UpdateProtectorField(gameTime);
                    }
                    player1.UpdateBullet(gameTime);
	            }

	            if (Controller.Instance.IsClient)
	            {
                    if (!player2.IsDestroyed()) 
                    { 
	                    player2.Update(gameTime);
	                    player2.UpdateProtectorField(gameTime);
                    }
                    player2.UpdateBullet(gameTime);

	                player1.UpdateBullet(gameTime);
	                player1.UpdateProtectorField(gameTime);
                    player1.UpdateImage(gameTime);
	            }

	            foreach (var enemy in enemies)
	            {
	                if (!enemy.IsDestroyed())
	                {
	                    if (Controller.Instance.IsSingle || Controller.Instance.IsServer)
	                    {
	                        enemy.Update(gameTime);
	                    }

                        if (Controller.Instance.IsClient)
                        {
                            enemy.UpdateImage(gameTime);
                        }
	                    
	                    enemy.DropPowerUpUpdate(gameTime);
	                }

	                enemy.UpdateBullet(gameTime);
	            }

	            foreach (var pu in powerUps)
	            {
	                if (!pu.IsDestroyed())
	                {
	                    pu.Update(gameTime);
	                }
	            }


	            ExplosionEngine.Instance.Update(gameTime);
			} catch(Exception ex) {
				Console.WriteLine ("[Update] " + ex.Message);
			}
        }

        public void Draw(SpriteBatch spriteBatch)
        {
			try {

	            background.Draw(spriteBatch);

	            List<BasicSprite> bushes = new List<BasicSprite>();

	            foreach (var element in stageElements)
	            {
	                if (element != null && !(element is Player))
	                {

	                    if (element is Bush)
	                    {
	                        bushes.Add(element);
	                    }
	                    else
	                    {
	                        element.Draw(spriteBatch);
	                    }
	                }
	            }

	            foreach (var border in borders)
	            {
	                border.Draw(spriteBatch);
	            }

	            player1.DrawBullet(spriteBatch);

	            if (Settings.TESZT || player1.Lives > 0)
	            {
	                player1.Draw(spriteBatch);
	            }

	            if (!Controller.Instance.IsSingle)
	            {
	                player2.DrawBullet(spriteBatch);

	                if (Settings.TESZT || player2.Lives > 0)
	                {
	                    player2.Draw(spriteBatch);
	                }


	            }

	            foreach (var enemy in enemies)
	            {
	                enemy.DrawBullet(spriteBatch);

	                if (!enemy.IsDestroyed())
	                {
	                    enemy.Draw(spriteBatch);
	                }
	                
	            }

	            foreach (var pu in powerUps)
	            {
	                if (!pu.IsDestroyed())
	                {
	                    pu.Draw(spriteBatch);
	                }
	            }

	            foreach (var bush in bushes)
	            {
	                bush.Draw(spriteBatch);
	            }

	            ExplosionEngine.Instance.Draw(spriteBatch);

                if (Controller.Instance.IsSingle || Controller.Instance.IsServer)
                {
                    spriteBatch.DrawString(Resources.Font, "Lives: " + player1.Lives, livesTextPos, textColor);
                }
                else
                {
                    spriteBatch.DrawString(Resources.Font, "Lives: " + player2.Lives, livesTextPos, textColor);
                }
                var meassure = Resources.Font.MeasureString("Remaining enemies: " + Controller.Instance.RemainingEnemies);
                var screenWidth = Game1.Instance.GraphicsDevice.Viewport.Width;
                var remTextPos = new Vector2(screenWidth - meassure.X - 10 - Settings.StageBorderSize, livesTextPos.Y);
                spriteBatch.DrawString(Resources.Font, "Remaining enemies: " + Controller.Instance.RemainingEnemies,remTextPos, textColor);

			} catch(Exception ex) {
				Console.WriteLine ("[Draw] " + ex.StackTrace);
			}
        }

        public Player Player1
        {
            get { return player1; }
        }

        public Player Player2
        {
            get { return player2; }
        }

        public List<BasicSprite> GetElementsCollidableByPlayer()
        {
            var list = new List<BasicSprite>();
            foreach (var element in stageElements)
            {
                if (element != null)
                {
                    list.Add(element);
                }

            }

            foreach (var border in borders)
            {
                list.Add(border);
            }

            return list;

        }

        public List<BasicSprite> GetElementsCollidableByBullet(bool enemy = false)
        {
            var list = new List<BasicSprite>();
            foreach (var element in stageElements)
            {
                if (element != null && !(element is Bush) && !(element is Water) && !(element is Icefield))
                {
                    list.Add(element);
                }

            }

            foreach (var e in enemies)
            {
                if (!e.IsDestroyed())
                {
                    list.Add(e);
                    if (e.Bullet != null)
                    {
                        list.Add(e.Bullet);
                    }
                }
            }

            if (!Controller.Instance.IsSingle)
            {
                if (player1.Bullet != null)
                {
                    list.Add(player1.Bullet);
                }
                if (player2.Bullet != null)
                {
                    list.Add(player2.Bullet);
                }
            }

            foreach (var border in borders)
            {
                list.Add(border);
            }

            return list;

        }

        public Cell EagleCellPosition
        {
            get;
            private set;
        }

        public void DrawForMenu(SpriteBatch spriteBatch)
        {
            background.Draw(spriteBatch);

            List<BasicSprite> bushes = new List<BasicSprite>();

            foreach (var element in stageElements)
            {
                if (element != null)
                {
                    element.Draw(spriteBatch);
                }
            }

            foreach (var border in borders)
            {
                border.Draw(spriteBatch);
            }
        }
    }
}
