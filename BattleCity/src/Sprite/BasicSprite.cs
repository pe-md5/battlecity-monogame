﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using BattleCity.Entity;

namespace BattleCity.Sprite
{

    /// <summary>
    /// This class defines a basic sprite with a single image with collision detection support
    /// </summary>
    /// 
    public delegate void CollisionEventHandler(BasicSprite other);

    public class BasicSprite 
    {

        /// <summary>
        /// Occurs when Collide with another sprite (called just once when the sprite enter)
        /// </summary>
        public event CollisionEventHandler CollisionEnterWith;
        
        /// <summary>
        /// Occurs when Collide with another sprite (called just once when the sprite exit)
        /// </summary>
        public event CollisionEventHandler CollisionExitWith;

        /// <summary>
        /// Occurs when Collide with another sprite
        /// </summary>
        public event CollisionEventHandler CollisionWith;

        /// <summary>
        /// Occurs when destroyed 
        /// </summary>
        public event EventHandler DestroyEvent;

        /// <summary>
        /// Store data for collision detection
        /// </summary>
        class CollisionRecord
        {
            public bool collidedWith;
            public BasicSprite sprite;

            public CollisionRecord(BasicSprite sprite)
            {
                collidedWith = false;
                this.sprite = sprite;
            }
        }
        /// <summary>
        /// The image to draw for this sprite
        /// </summary>
        SpriteImage image;


        /// <summary>
        /// Color for drawing
        /// </summary>
        protected Color color;

        /// <summary>
        /// Position of the image
        /// </summary>
        protected Vector2 position;

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// This flag shows that we need to check this object in 
        /// another BasicSprite object for collision detection or not
        /// </summary>
        bool destroyed = false;

        /// <summary>
        /// List of other sprites to check for collision with
        /// </summary>
        List<CollisionRecord> collisionList = new List<CollisionRecord>();


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="image">Texture of the sprite</param>
        /// <param name="position">Position of the sprite</param>
        public BasicSprite(Texture2D image, Vector2 position, Color color)
        {
            this.image = new SimpleSpriteImage(image);
            this.position = position;
            this.color = color;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="simage">Image of the sprite</param>
        /// <param name="position">Position of the sprite</param>
        public BasicSprite(SpriteImage simage, Vector2 position, Color color)
        {
            this.image = simage;
            this.position = position;
            this.color = color;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="image">Texture of the sprite</param>
        /// <param name="position">Position of the sprite</param>
        public BasicSprite(Texture2D image, Vector2 position)
        {
            this.image = new SimpleSpriteImage(image);
            this.position = position;
            color = Color.White;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="simage">Image of the sprite</param>
        /// <param name="position">Position of the sprite</param>
        public BasicSprite(SpriteImage simage, Vector2 position)
        {
            this.image = simage;
            this.position = position;
            color = Color.White;
        }

        public BasicSprite()
        {
        }

        /// <summary>
        /// Sets the image of the sprite.
        /// </summary>
        /// <param name="simage">The image to set</param>
        protected void SetImage(SpriteImage simage)
        {
            this.image = simage;
        }
       
        /// <summary>
        /// Gets the image of the sprite.
        /// </summary>
        /// <returns>The image of the sprite</returns>
        public SpriteImage GetSpriteImage()
        {
            return image;
        }

        bool collisionCheck = true;

        public void StopCollisionCheck()
        {
            collisionCheck = false;
        }

        /// <summary>
        /// Gets the size of the sprite (the image)
        /// </summary>
        /// <returns></returns>
        public Vector2 GetSize()
        {
            if (image == null)
            {
                return Vector2.Zero;
            }
            else
            {
                return image.GetCurrentImageSize();
            }
        }


        /// <summary>
        /// Gets a rectangle that represents the collider of this sprite.
        /// </summary>
        /// <returns></returns>
        public virtual Rectangle GetCollider()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)image.GetCurrentImageSize().X, (int)image.GetCurrentImageSize().Y);
        }

        /// <summary>
        /// Called when this sprite collides with another
        /// </summary>
        /// <param name="other">The other sprite</param>
        public virtual void OnCollisionWith(BasicSprite other)
        {
            if (CollisionWith != null)
            {
                CollisionWith(other);
            }
        }

        /// <summary>
        /// Called when this sprite collides with another (called just once when the sprite enter)
        /// </summary>
        /// <param name="other">The other sprite</param>
        public virtual void OnCollisionEnterWith(BasicSprite other)
        {
            if (CollisionEnterWith != null)
            {
                CollisionEnterWith(other);
            }
        }

        /// <summary>
        /// Called when this sprite collides with another (called just once when the sprite exit)
        /// </summary>
        /// <param name="other">The other sprite</param>
        public virtual void OnCollisionExitWith(BasicSprite other)
        {
            if (CollisionExitWith != null)
            {
                CollisionExitWith(other);
            }
        }

        /// <summary>
        /// Add a sprite to the collision list to check collision.
        /// </summary>
        /// <param name="other">The sprite to check with collision</param>
        public void CollidesWith(BasicSprite other)
        {
            if (other != this) 
            { 
                collisionList.Add(new CollisionRecord(other));
            }
        }

        /// <summary>
        /// Draws the sprite
        /// </summary>
        /// <param name="batch">The SpriteBatch object</param>
        public virtual void Draw(SpriteBatch batch)
        {
            image.Draw(batch, position, color);
        }

        /// <summary>
        /// Updates the sprite (check collision detection)
        /// </summary>
        /// <param name="gameTime">The current GameTime object</param>
        public virtual void Update(GameTime gameTime)
        {
            if (image != null)
            {
                image.Update(gameTime);
            }
            List<CollisionRecord> removalList= new List<CollisionRecord>();
            for (int i = 0; i < collisionList.Count; i++)
            {
                var rec = collisionList.ElementAt(i);

                if (collisionCheck)
                {
                    BasicSprite other = rec.sprite;
                    if (other.IsDestroyed())
                    {
                        removalList.Add(rec);
                        i++;
                    }
                    else
                    {
                        if (GetCollider().Intersects(other.GetCollider()))
                        {
                            if (!rec.collidedWith)
                            {
                                rec.collidedWith = true;
                                OnCollisionEnterWith(other);
                            }
                            OnCollisionWith(other);
                        }
                        else
                        {
                            if (rec.collidedWith)
                            {
                                rec.collidedWith = false;
                                OnCollisionExitWith(other);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }

            }

            foreach (CollisionRecord r in removalList)
            {
                collisionList.Remove(r);
            }
        }

        public void UpdateImage(GameTime gameTime)
        {
            image.Update(gameTime);
        }


        /// <summary>
        /// Check collision manually.
        /// </summary>
        /// <returns></returns>
        public bool CheckCollision()
        {
            Rectangle collider = GetCollider();
            bool collision = false;

            List<CollisionRecord> removalList = new List<CollisionRecord>();
            foreach (CollisionRecord rec in collisionList)
            {
                BasicSprite other = rec.sprite;
                if (other.IsDestroyed())
                {
                    removalList.Add(rec);
                }
                else
                {
                    if (collider.Intersects(other.GetCollider()))
                    {
                        if (!rec.collidedWith)
                        {
                            rec.collidedWith = true;
                        }
                        collision = true;
                    }
                }
            }
            foreach (CollisionRecord r in removalList)
            {
                collisionList.Remove(r);
            }

            return collision;
        }

        /// <summary>
        /// Called after the Destroy() method.
        /// </summary>
        public virtual void OnDestroy()
        {
            if (DestroyEvent != null)
            {
                DestroyEvent(this, null);
            }
        }

        /// <summary>
        /// Sets the destroyed flag to true and clear the collisionlist
        /// </summary>
        public void Destroy()
        {
            destroyed = true;
            OnDestroy();
        }

        /// <summary>
        /// Gets the destroyed flag
        /// </summary>
        /// <returns>The destroyed flag</returns>
        public bool IsDestroyed()
        {
            return destroyed;
        }

    }
}
