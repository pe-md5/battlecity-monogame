﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using BattleCity.Sprite;


namespace BattleCity.Sprite
{
    public class MultiImageSprite:BasicSprite 
    {
        private SpriteImage[] images;

        public MultiImageSprite(SpriteImage[] simage, Vector2 position) : base(simage[0], position) {
            images = simage;
            imageIdx = 0;
        }

        public MultiImageSprite(SpriteImage[] simage, Vector2 position, Color color)
            : base(simage[0], position, color)
        {
            images = simage;
            imageIdx = 0;
        }

        public int GetImageCount()
        {
            return images.Length;
        }

        public void SetCurrentImage(int idx)
        {
            SetImage(images[idx]);
            imageIdx = idx;
        }

        int imageIdx;


        public int ImageIdx
        {
            get { return imageIdx; }
        }
    }
}
