using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization; // Required Reference: System.Web.Extensions
using Lidgren.Network;
using BattleCity.Network.Events;

namespace BattleCity.Network
{
	
	/// <summary>
	/// Acting as Server, accepting connections.
	/// </summary>
	public class NetworkServer
	{
		private NetServer server;
		private NetPeerConfiguration config;

		public NetworkServer (int port)
		{
            if (SynchronizationContext.Current == null)
                SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

			config = new NetPeerConfiguration("battlecity");
			config.EnableUPnP = true;
			config.Port = port;
			config.MaximumConnections = 1;
			config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
		}

		public void Start()
		{
			server = new NetServer(config);
			server.RegisterReceivedCallback(new SendOrPostCallback(HandleMessages));
			server.Start();
		}

		public void Stop()
		{
			server.Shutdown("Server closed");
		}

		public bool IsConnected()
		{
			return server.Connections.Count > 0;
		}

		public void SendEvent(NetworkEvent e)
		{
			string data = NetworkHandler.SerializeNetworkEvent (e);
			foreach (NetConnection player in server.Connections)
			{
				NetworkHandler.Send (server, player, data);
			}
		}

		public void HandleMessages(object fromPlayer)
		{
			NetIncomingMessage msg;
			if ((msg = ((NetServer)fromPlayer).ReadMessage()) == null) return;

			switch (msg.MessageType)
			{
				case NetIncomingMessageType.DiscoveryRequest:
				//
				// Server received a discovery request from a client; send a discovery response (with no extra data attached)
				//
                    if (server.ConnectionsCount == 0) { 
				        server.SendDiscoveryResponse(null, msg.SenderEndpoint);
                    }
                    Console.WriteLine("NetworkServer: Discovery Request");
				break;
				case NetIncomingMessageType.VerboseDebugMessage:
				case NetIncomingMessageType.DebugMessage:
				case NetIncomingMessageType.WarningMessage:
				case NetIncomingMessageType.ErrorMessage:
				//
				// Just print diagnostic messages to console
				//
				Console.WriteLine(msg.ReadString());
				break;
				case NetIncomingMessageType.StatusChanged:
				NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
				if (status == NetConnectionStatus.Connected)
				{
					Console.WriteLine("NetworkServer: " + NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " connected!");
				} 
                else if(status == NetConnectionStatus.Disconnected)
                {
                    Console.WriteLine("Client disconnected. Close the game...");
                    Game1.Instance.State = GameState.MainMenu;
                    Controller.Instance.StopServer();
                    Controller.Instance.StageIndex = 0;
                }
				break;
				case NetIncomingMessageType.Data:
					NetworkHandler.HandleNetworkEvent(msg);
				break;
			}
		}
	}
}

