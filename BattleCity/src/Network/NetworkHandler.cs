using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Script.Serialization; // Required Reference: System.Web.Extensions
using Lidgren.Network;
using BattleCity.Network.Events;
using BattleCity.Entity;

namespace BattleCity.Network
{
	/// <summary>
	/// Building network connection(s) with other player(s).
	/// </summary>
	public class NetworkHandler
	{
		public static void HandleNetworkEvent(NetIncomingMessage msg)
		{
			NetworkEvent e = DeserializeNetworkEvent(msg);
			switch (e.type) {
				case NetworkEvent.NetworkEventType.PlayerMovingEvent:
					HandleNetworkPlayerMovingEvent((NetworkPlayerMovingEvent)e);
				    break;
				case NetworkEvent.NetworkEventType.StageEvent:
                    HandleNetworkStageEvent((NetworkStageEvent)e);
				    break;
                case NetworkEvent.NetworkEventType.EnemySpawnEvent:
                    HandleNetworkEnemySpawnEvent((NetworkEnemySpawnEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.EnemyMovingEvent:
                    HandleNetworkEnemyMovingEvent((NetworkEnemyMovingEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.EnemyDead:
                    HandleNetworkEnemyDeadEvent((NetworkEnemyDeadEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.PlayerShootingEvent:
                    HandleNetworkPlayerShootingEvent((NetworkPlayerShootingEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.PBulletCollideEvent:
                    HandleNetworkPBulletCollideEvent((NetworkPBulletCollideEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.EnemyShootingEvent:
                    HandleNetworkEnemyShootingEvent((NetworkEnemyShootingEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.EnemyBulletCollideEvent:
                    HandleNetworkEnemyBulletCollideEvent((NetworkEnemyBulletCollideEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.PowerUpGainEvent:
                    HandleNetworkPowerUpGainEvent((NetworkPowerUpGainEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.PlayerProtectEvent:
                    HandleNetworkPowerUpGainEvent((NetworkPlayerProtectEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.PlayerHideEvent:
                    HandleNetworkPlayerHideEvent((NetworkPlayerHideEvent)e);
                    break;
                case NetworkEvent.NetworkEventType.PlayerDamagedEvent:
                    HandleNetworkPlayerDamaged((NetworkPlayerDamagedEvent)e);
                    break;
			}
		}

        private static void HandleNetworkPlayerDamaged(NetworkPlayerDamagedEvent e)
        {
            if (e.id == 1)
            {
                Controller.Instance.Player1.UpdateDamagedFromPartner();
            }
            else if (e.id == 2)
            {
                Controller.Instance.Player2.UpdateDamagedFromPartner();
            }
        }

        private static void HandleNetworkPlayerHideEvent(NetworkPlayerHideEvent e)
        {
            if (e.id == 1)
            {
                Controller.Instance.Player1.UpdateHideFromPartner(e);
            }
            else if (e.id == 2)
            {
                Controller.Instance.Player2.UpdateHideFromPartner(e);
            }
        }

        private static void HandleNetworkPowerUpGainEvent(NetworkPlayerProtectEvent e)
        {
            if (e.id == 1)
            {
                Controller.Instance.Player1.UpdateProtectorFieldFromPartner(e);
            }
            else if (e.id == 2)
            {
                Controller.Instance.Player2.UpdateProtectorFieldFromPartner(e);
            }
        }

        private static void HandleNetworkPowerUpGainEvent(NetworkPowerUpGainEvent e)
        {
            Controller.Instance.PowerUps[e.powerUpidx].Destroy();
            Controller.Instance.HandleGlobalPowerUpEffect(e.powerUptype);
        }

        private static void HandleNetworkEnemyBulletCollideEvent(NetworkEnemyBulletCollideEvent e)
        {
            Controller.Instance.Enemies[e.id].UpdateBulletCollideFromServer();
        }

        private static void HandleNetworkEnemyShootingEvent(NetworkEnemyShootingEvent e)
        {
            Controller.Instance.Enemies[e.id].UpdateShootingFromServer();
        }

        private static void HandleNetworkPBulletCollideEvent(NetworkPBulletCollideEvent e)
        {
            if (e.id == 1)
            {
                Controller.Instance.Player1.UpdatePBulletCollideFromPartner();
            }
            else if (e.id == 2)
            {
                Controller.Instance.Player2.UpdatePBulletCollideFromPartner();
            }
        }

        private static void HandleNetworkPlayerShootingEvent(NetworkPlayerShootingEvent e)
        {
            if (e.id == 1)
            {
                Controller.Instance.Player1.UpdateShootingFromPartner(e);
            }
            else if (e.id == 2)
            {
                Controller.Instance.Player2.UpdateShootingFromPartner(e);
            }
        }

        private static void HandleNetworkEnemyDeadEvent(NetworkEnemyDeadEvent e)
        {
            Controller.Instance.Enemies[e.id].UpdateDamagedFromPartner();
        }

        private static void HandleNetworkStageEvent(NetworkStageEvent e)
        {
            Controller.Instance.UpdateStageEventFromPartner(e);
        }

        private static void HandleNetworkPlayerMovingEvent(NetworkPlayerMovingEvent e)
        {
            if (e.id == 1)
            {
                Controller.Instance.Player1.UpdateMovingFromPartner(e);
            }
            else if(e.id == 2)
            {
                Controller.Instance.Player2.UpdateMovingFromPartner(e);
            }
        }

        private static void HandleNetworkEnemyMovingEvent(NetworkEnemyMovingEvent e)
        {
			if (Controller.Instance.Enemies.Count > e.idx) {
				Controller.Instance.Enemies [e.idx].UpdateMovingFromServer (e);
			} else {
				Console.WriteLine ("FAIL: NetworkHandler.HandleNetworkEnemyMovingEvent");
			}
        }

        private static void HandleNetworkEnemySpawnEvent(NetworkEnemySpawnEvent e)
        {
            var enemy = new Enemy(e.cellPosition, e.enemyType);
            enemy.DropPowerUp = e.dropPowerUp;
            enemy.PowerUpType = e.powerUpType;
            Controller.Instance.Player2.CollidesWith(enemy);
            enemy.DestroyEvent += Controller.Instance.enemy_DestroyEvent;
        }

		/// <summary>
		/// Sending NetworkEvent from source to dest.
		/// </summary>
		public static void SendNetworkEvent(NetPeer src, NetConnection dest, NetworkEvent e)
		{
			Send (src, dest, SerializeNetworkEvent(e));
		}

		/// <summary>
		/// Sending NetworkEvent object Serialized to string.
		/// </summary>
		public static void Send(NetPeer src, NetConnection dest, string data)
		{
			NetOutgoingMessage ms = src.CreateMessage ();
			//Console.WriteLine ("Send: " + data);
			ms.Write (data);
			src.SendMessage (ms, dest, NetDeliveryMethod.ReliableOrdered);
		}

		/// <summary>
		/// Serializes a network event for sending.
		/// </summary>
		public static string SerializeNetworkEvent(NetworkEvent e)
		{
			var serializer = new JavaScriptSerializer();
			return serializer.Serialize(e);
		}

		/// <summary>
		/// Deserialize received Data.
		/// </summary>
		public static NetworkEvent DeserializeNetworkEvent(NetIncomingMessage msg)
		{
			string input = msg.ReadString ();
			var serializer = new JavaScriptSerializer();
			NetworkEvent e = serializer.Deserialize<NetworkEvent>(input);
			switch (e.type) {
				case NetworkEvent.NetworkEventType.PlayerMovingEvent:
					e = serializer.Deserialize<NetworkPlayerMovingEvent>(input);
				    break;
				case NetworkEvent.NetworkEventType.StageEvent:
					e = serializer.Deserialize<NetworkStageEvent>(input);
				    break;
                case NetworkEvent.NetworkEventType.EnemySpawnEvent:
                    e = serializer.Deserialize<NetworkEnemySpawnEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.EnemyMovingEvent:
                    e = serializer.Deserialize<NetworkEnemyMovingEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.EnemyDead:
                    e = serializer.Deserialize<NetworkEnemyDeadEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.PlayerShootingEvent:
                    e = serializer.Deserialize<NetworkPlayerShootingEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.PBulletCollideEvent:
                    e = serializer.Deserialize<NetworkPBulletCollideEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.EnemyBulletCollideEvent:
                    e = serializer.Deserialize<NetworkEnemyBulletCollideEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.EnemyShootingEvent:
                    e = serializer.Deserialize<NetworkEnemyShootingEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.PowerUpGainEvent:
                    e = serializer.Deserialize<NetworkPowerUpGainEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.PlayerProtectEvent:
                    e = serializer.Deserialize<NetworkPlayerProtectEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.PlayerHideEvent:
                    e = serializer.Deserialize<NetworkPlayerHideEvent>(input);
                    break;
                case NetworkEvent.NetworkEventType.PlayerDamagedEvent:
                    e = serializer.Deserialize<NetworkPlayerDamagedEvent>(input);
                    break;
			}
			return e;
		}
	}
}

