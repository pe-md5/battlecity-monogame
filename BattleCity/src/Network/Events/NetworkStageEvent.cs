using BattleCity.AI;
using System;

namespace BattleCity.Network.Events
{
	public class NetworkStageEvent : NetworkEvent
	{
		public NetworkStageEvent():base(NetworkEventType.StageEvent) { }

		/// <summary>
		/// Location in the 2D Matrix (Stage Map)
		/// </summary>
        public Cell cellPos;
        public int decreaseValue;

	}
}

