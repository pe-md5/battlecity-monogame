﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkEnemyBulletCollideEvent : NetworkEvent
    {
        public NetworkEnemyBulletCollideEvent()
            : base(NetworkEventType.EnemyBulletCollideEvent)
        {
            
        }

        public int id;
    }
}
