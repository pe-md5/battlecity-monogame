﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkEnemyDeadEvent : NetworkEvent
    {
        public NetworkEnemyDeadEvent()
            : base(NetworkEventType.EnemyDead)
        {

        }

        public int id;
    }
}
