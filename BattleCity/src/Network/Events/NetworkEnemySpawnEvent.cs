﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Entity;
using Microsoft.Xna.Framework;
using BattleCity.AI;
using BattleCity.StageElements;

namespace BattleCity.Network.Events
{
    class NetworkEnemySpawnEvent : NetworkEvent
    {
        public NetworkEnemySpawnEvent():base(NetworkEventType.EnemySpawnEvent) { }

        public Enemy.EnemyType enemyType;
        public Cell cellPosition;
        public bool dropPowerUp;
        public PowerUp.PTypes powerUpType;
    }
}
