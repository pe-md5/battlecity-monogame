﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkPlayerHideEvent : NetworkEvent
    {
        public NetworkPlayerHideEvent()
            : base(NetworkEventType.PlayerHideEvent)
        {

        }

        public int id;
        public bool hide;
    }
}
