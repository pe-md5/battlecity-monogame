﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkPlayerProtectEvent : NetworkEvent
    {
        public NetworkPlayerProtectEvent()
            : base(NetworkEventType.PlayerProtectEvent)
        {

        }

        public int id;
        public bool protect;
    }
}
