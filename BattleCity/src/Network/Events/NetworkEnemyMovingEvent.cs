﻿using BattleCity.Entity;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkEnemyMovingEvent : NetworkEvent
    {
        public NetworkEnemyMovingEvent():base(NetworkEventType.EnemyMovingEvent) { }

        public int idx;

		public Vector2 _position;
		public Vector2 position {
			get {
				return _position;
			}
			set {
				_position = value;
				_position.X = (float)Math.Round (_position.X);
				_position.Y = (float)Math.Round (_position.Y);
			}
		}
        public Directions direciton;
        public bool moving;
    }
}
