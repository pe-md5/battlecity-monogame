﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkPlayerDamagedEvent : NetworkEvent
    {
        public NetworkPlayerDamagedEvent()
            : base(NetworkEventType.PlayerDamagedEvent)
        {

        }

        public int id;
    }
}
