﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.StageElements;

namespace BattleCity.Network.Events
{
    public class NetworkPowerUpGainEvent : NetworkEvent
    {
        public NetworkPowerUpGainEvent()
            : base(NetworkEventType.PowerUpGainEvent)
        {

        }

        public int powerUpidx;

        public PowerUp.PTypes powerUptype;
    }
}
