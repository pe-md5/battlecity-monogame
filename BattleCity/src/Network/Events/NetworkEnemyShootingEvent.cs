﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkEnemyShootingEvent : NetworkEvent
    {
        public NetworkEnemyShootingEvent() : base(NetworkEventType.EnemyShootingEvent) { }

        public int id;
    }
}
