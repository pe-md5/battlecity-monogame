﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkPlayerShootingEvent : NetworkEvent
    {
        public NetworkPlayerShootingEvent():base(NetworkEventType.PlayerShootingEvent) { }

        public int id;
    }
}
