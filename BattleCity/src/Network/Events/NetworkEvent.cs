using System;

namespace BattleCity.Network.Events
{
	/// <summary>
	/// NetworkEvent State
	/// </summary>
	public class NetworkEvent
	{
        public enum NetworkEventType { StageEvent, EnemySpawnEvent, EnemyDead, PlayerMovingEvent, PlayerShootingEvent, PBulletCollideEvent, PlayerCollideEvent, EnemyShootingEvent, EnemyBulletCollideEvent, EnemyMovingEvent, PowerUpGainEvent, PowerUpSpawnEvent, PlayerProtectEvent, PlayerHideEvent, PlayerDamagedEvent }
		public NetworkEventType type;

        public NetworkEvent(NetworkEventType type)
        {
            this.type = type;
        }

        public NetworkEvent()
        {

        }
	}
}

