using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using BattleCity.Entity;

namespace BattleCity.Network.Events
{
	public class NetworkPlayerMovingEvent : NetworkEvent
	{
		public NetworkPlayerMovingEvent():base(NetworkEventType.PlayerMovingEvent) { }

        public int id;

        public Vector2 _position;
		public Vector2 position {
			get {
				return _position;
			}
			set {
				_position = value;
				_position.X = (float)Math.Round (_position.X);
				_position.Y = (float)Math.Round (_position.Y);
			}
		}
		
        public Directions direciton;
        public bool moving;
    }
}

