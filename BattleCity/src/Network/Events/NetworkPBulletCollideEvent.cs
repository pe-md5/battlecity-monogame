﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Network.Events
{
    public class NetworkPBulletCollideEvent : NetworkEvent
    {
        public int id;
        public NetworkPBulletCollideEvent():base(NetworkEventType.PBulletCollideEvent) { }
    }
}
