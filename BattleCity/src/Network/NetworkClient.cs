using System;
using System.Threading;
using Lidgren.Network;
using System.Net;
using System.Collections.Generic;
using BattleCity.GUI;
using BattleCity.Network.Events;

namespace BattleCity.Network
{
	
	/// <summary>
	/// Acting as Client, connecting to server.
	/// </summary>
	public class NetworkClient
	{
		NetClient client;
		int port;

        public void Connect(IPAddress address)
        {
            client.Connect(new IPEndPoint(address, port));
        }

		public NetworkClient (int port)
		{
            if (SynchronizationContext.Current == null)
                SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

			this.port = port;
			
			if (SynchronizationContext.Current == null)
				SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

			NetPeerConfiguration config = new NetPeerConfiguration("battlecity");
			config.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);

			client = new NetClient(config);
			client.RegisterReceivedCallback(new SendOrPostCallback(HandleMessages));
			client.Start();
		}

        public void SendEvent(NetworkEvent e)
        {
            string data = NetworkHandler.SerializeNetworkEvent(e);
            NetworkHandler.Send(client, client.ServerConnection, data);
        }

        public void Discover()
        {
            Console.WriteLine("NetworkClient: Discovering");
            client.DiscoverLocalPeers(port);
        }

		public void Stop()
		{
            client.Disconnect("");
            client.Shutdown("");
		}

		public Boolean IsConnected() 
		{
            return client.ConnectionStatus == NetConnectionStatus.Connected;
		}

		public void HandleMessages(object fromServer)
		{
			NetIncomingMessage msg;
            
			if ((msg = client.ReadMessage()) == null) return;

			switch (msg.MessageType)
			{
                case NetIncomingMessageType.DiscoveryResponse:
                    Console.WriteLine("Network Client: Discovered " + msg.SenderEndpoint.ToString());
                    DiscoverMenu.Instance.IPList.Add(msg.SenderEndpoint.Address.ToString());
                    break;
				case NetIncomingMessageType.Data:
					NetworkHandler.HandleNetworkEvent(msg);
				break;
                case NetIncomingMessageType.StatusChanged:
                    NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();

                    if (status == NetConnectionStatus.Disconnected) 
                    { 
                        Game1.Instance.State = GameState.MainMenu;
                        Controller.Instance.StopClient();
                        Controller.Instance.StageIndex = 0;
                    }

                    break;
			}
		}
	}
}

