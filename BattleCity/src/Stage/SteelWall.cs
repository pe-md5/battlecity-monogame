﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;
using BattleCity.AI;

namespace BattleCity.StageElements
{
    public class SteelWall : MultiImageSprite
    {
        int shotsCount = 5;

        Cell cellPos;

        public Cell CellPosition
        {
            get { return cellPos; }
        }

        public SteelWall(SimpleSpriteImage[] simage, Vector2 position, Cell cellPos)
            : base(simage, position)
        {
            this.cellPos = cellPos;
        }

        public void DecrementShotsCount(int value)
        {

            shotsCount -= value;

            if (shotsCount > 0)
            {
                SetCurrentImage(5 - shotsCount);
            }

            if (shotsCount <= 0)
            {
                Destroy();
            }

        }


    }
}
