﻿using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Utils;

namespace BattleCity.StageElements
{
    public class PowerUp : BasicSprite
    {
        public enum PTypes { TANK, STAR, BOMB, CLOCK, SHIELD }

        public PTypes PType
        {
            get;
            private set;
        }

        public PowerUp(PTypes type, Vector2 position, int id)
            : base(Resources.PowerUpImages[(int)type], position)
        {
            PType = type;
            Id = id;
        }

        public int Id
        {
            get;
            set;
        }

        

    }
}
