﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;
using BattleCity.AI;

namespace BattleCity.StageElements
{

    public class BrickWall : MultiImageSprite
    {
        int shotsCount = 5;

        Cell cellPos;

        public Cell CellPosition
        {
            get { return cellPos; }
        }

        public BrickWall(SimpleSpriteImage[] simage, Vector2 position, Cell cellPos) : base(simage, position)
        {
            this.cellPos = cellPos;
            cellPos.IsWall = true;
        }

        public void DecrementShotsCount(int value)
        {
            
            shotsCount -= value;

            if (shotsCount > 0)
            {
                SetCurrentImage(5 - shotsCount);
            }
            
            if (shotsCount <= 0)
            {
                Destroy();
            }

        }

    }
}
