﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;

namespace BattleCity.StageElements
{
    public class Bush : BasicSprite
    {
        public Bush(SimpleSpriteImage image, Vector2 position):base(image, position) 
        {

        }

    }
}
