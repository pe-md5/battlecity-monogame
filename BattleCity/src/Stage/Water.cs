﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;

namespace BattleCity.StageElements
{
    public class Water : BasicSprite
    {
        public Water(AnimatedSpriteImage image, Vector2 position)
            : base(image, position)
        {

        }

    }
}
