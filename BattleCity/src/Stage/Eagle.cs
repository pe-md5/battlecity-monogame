﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Sprite;
using BattleCity.AI;

namespace BattleCity.StageElements
{
    public class Eagle : MultiImageSprite
    {
 
        public Eagle(SimpleSpriteImage[] image, Vector2 position, Cell cellPos)
            : base(image, position)
        {
            ShotsCount = 3;
            CellPosition = cellPos;
            CellPosition.IsWall = true;
        }


        public void DecrementShotsCount(int value)
        {

            ShotsCount -= value;

            if (ShotsCount >= 0)
            {
                SetCurrentImage(3 - ShotsCount);
            }

            if (ShotsCount <= 0)
            {
                Destroy();
            }
        }



        public Cell CellPosition
        {
            get;
            set;
        }

        public int ShotsCount
        {
            get;
            set;
        }
    }
}
