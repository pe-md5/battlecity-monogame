﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Entity;
using BattleCity.GUI;
using BattleCity.Sprite;
using BattleCity.Effect;
using BattleCity.AI;
using BattleCity.Network;
using System.Net;
using System.Threading;
using BattleCity.Network.Events;
using BattleCity.StageElements;
using BattleCity.Utils;

namespace BattleCity
{
    public class Controller
    {

        NetworkServer server;
        NetworkClient client;

        float broadCastTimeOut = 1/30;
        float elapsedGameTimeForBroadCast = 0.0f;

        public void DiscoverServers()
        {
            if (client == null)
            {
                client = new NetworkClient(Settings.Port);
            }
            client.Discover();
        }

        public void StopClient()
        {
            if (client != null)
            {
                client.Stop();
            }
            client = null;
            Game1.Instance.ClearLoadingScreen();
        }

        public void ConnectToServer(IPAddress address)
        {
            client.Connect(address);
            Game1.Instance.State = GameState.InMP;
            DiscoverMenu.Instance.ClearIPList();
        }

        Stage currentStage;
        int stageIdx;

        Player player1;
        Player player2;

        bool pause = false;

        List<Enemy> enemies;
        List<PowerUp> powerUps;

        Random r = new Random();

        int numberOfEasyEnemies;
        int numberOfMediumEnemies;
        int numberOfHardEnemies;

        int enemyKilled;

        public int RemainingEnemies
        {
            get { return 20 - enemyKilled; }
        }


        float enemySpawnTimeOut;

        float elapsedGameTime;

        int numberOfEnemiesSpawned;

        private Controller()
        {
            stageIdx = 0;
        }

        public void StartServer()
        {
            server = new NetworkServer(Settings.Port);
            server.Start();
        }

        public void StopServer()
        {
            if (server != null)
            {
                server.Stop();
            }
            server = null;
            
            Game1.Instance.ClearLoadingScreen();
        }

        public int StageIndex
        {
            get { return stageIdx; }
            set { stageIdx = value; }
        }

        public Stage CurrentStage
        {
            get { return currentStage; }
        }

        public bool IsServer
        {
            get 
            {
                return server != null;
            }
        }

        public bool IsSingle
        {
            get
            {
                return server == null && client == null;
            }
        }

        public bool IsClient
        {
            get
            {
                return client != null;
            }
        }

        public void SendNetworkEvent(NetworkEvent e)
        {
            if (IsServer)
            {
                server.SendEvent(e);
            }
            else
            {
                client.SendEvent(e);
            }
        }

        public Player Player1
        {
            get { return player1; }
        }

        public Player Player2
        {
            get { return player2; }
        }

        public bool ClientConnected
        {
            get { return server.IsConnected(); }
        }

        public void Pause()
        {
            pause = true;
        }

        public void Resume()
        {
            pause = false;
        }

        static Controller instance;
        private bool victory;
        private bool gameOver;
        

        public static Controller Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Controller();
                }
                return instance;
            }
        }

        public void LoadStage()
        {
            enemies = new List<Enemy>();
            powerUps = new List<PowerUp>();

            currentStage = new Stage(this, StageDescriptions.Get(stageIdx), enemies, powerUps);

            player1 = currentStage.Player1;

            if (IsServer || IsSingle)
            {
                player1.Shoot += player1_Shoot;
                player1.CollisionWith += player_CollisionWith;
            }

            if (!IsSingle)
            {
                player2 = currentStage.Player2;
                player1.CollidesWith(player2);
            }

            if (IsClient)
            {
                player2.CollidesWith(player1);
                player2.Shoot += player2_Shoot;
                player2.CollisionWith += player_CollisionWith;
            }

            var list = currentStage.GetElementsCollidableByPlayer();
            
            if (IsSingle || IsServer)
            {
                foreach (var element in list)
                {
                    player1.CollidesWith(element);
                }
            }

            if (IsClient)
            {
                foreach (var element in list)
                {
                    player2.CollidesWith(element);
                }
            }

            InitializeStageParameters();
        }

        private void InitializeStageParameters()
        {
            victory = false;
            gameOver = false;
            enemyKilled = 0;
            enemySpawnTimeOut = Settings.InitialSpawnTimeOut - stageIdx * Settings.SpawnTimeOutDecreaseValue;

            if (stageIdx >= 0 && stageIdx < 3)
            {
                int mediumAndHardEnemies = (int)Math.Round(((double)(stageIdx + 1) / 30) * 20);

                numberOfMediumEnemies = (int)Math.Round(mediumAndHardEnemies * Settings.MediumEnemyRatioBetween1And10);

                numberOfHardEnemies = mediumAndHardEnemies - numberOfMediumEnemies;
                numberOfEasyEnemies = 20 - numberOfHardEnemies - numberOfMediumEnemies;
            } 
            else if(stageIdx >= 3 && stageIdx < 6) 
            {
                int mediumAndHardEnemies = (int)Math.Round(((double)(stageIdx + 1) / 30) * 20);

                numberOfMediumEnemies = (int)Math.Round(mediumAndHardEnemies * Settings.MediumEnemyRatioBetween11And20);

                numberOfHardEnemies = mediumAndHardEnemies - numberOfMediumEnemies;
                numberOfEasyEnemies = 20 - numberOfHardEnemies - numberOfMediumEnemies;
            }
            else if (stageIdx >= 6 && stageIdx < 9)
            {
                int mediumAndHardEnemies = (int)Math.Round(((double)(stageIdx + 1) / 20) * 20);

                numberOfMediumEnemies = (int)Math.Round(mediumAndHardEnemies * Settings.MediumEnemyRatioBetween21And30);

                numberOfHardEnemies = mediumAndHardEnemies - numberOfMediumEnemies;
                numberOfEasyEnemies = 20 - numberOfHardEnemies - numberOfMediumEnemies;
            }
            else if (stageIdx == 9)
            {
                int mediumAndHardEnemies = (int)Math.Round(((double)(stageIdx + 1) / 20) * 20);

                numberOfMediumEnemies = (int)Math.Round(mediumAndHardEnemies * Settings.MediumEnemyRatioBetween31And35);

                numberOfHardEnemies = mediumAndHardEnemies - numberOfMediumEnemies;
                numberOfEasyEnemies = 20 - numberOfHardEnemies - numberOfMediumEnemies;
            }

            numberOfEnemiesSpawned = 0;
        }

        void player1_Shoot(object sender, EventArgs e)
        {
            var list = currentStage.GetElementsCollidableByBullet();
            foreach (var element in list)
            {
                player1.Bullet.CollidesWith(element);
            }

            if (!IsSingle)
            {
                player1.Bullet.CollidesWith(player2);
            }
        }


        void player_CollisionWith(BasicSprite other)
        {
            if (other is PowerUp)
            {
                HandleGlobalPowerUpEffect((other as PowerUp).PType);
            }
            
        }

        public void HandleGlobalPowerUpEffect(PowerUp.PTypes type)
        {
                switch (type)
                {
                    case PowerUp.PTypes.BOMB:
                        // Destroy all enemies
                        foreach (var en in enemies)
                        {
                            if (!en.IsDestroyed())
                            {
                                var enemySize = en.GetSpriteImage().GetCurrentImageSize();
                                var explSize = new Vector2(Resources.ExplosionTexture.Width / Settings.NumberOfExplosionFrames, Resources.ExplosionTexture.Height);
                                var explPos = new Vector2(en.Position.X + (enemySize.X - explSize.X) / 2, en.Position.Y + (enemySize.Y - explSize.Y) / 2);
                                ExplosionEngine.Instance.Add(explPos);
                                en.Destroy();
                                if (!Controller.Instance.IsSingle)
                                {
                                    SendNetworkEvent(new NetworkEnemyDeadEvent() { id = en.EnemyIdx });
                                }
                            }
                        }
                        break;
                    case PowerUp.PTypes.CLOCK:
                        // Freeze all enemies
                        foreach (var en in enemies)
                        {
                            if (!en.IsDestroyed())
                            {
                                en.Freeze();
                            }
                        }
                        break;
                }
        }


        void player2_Shoot(object sender, EventArgs e)
        {
            var list = currentStage.GetElementsCollidableByBullet();
            foreach (var element in list)
            {
                player2.Bullet.CollidesWith(element);
            }

            player2.Bullet.CollidesWith(player1);
        }

        public void enemy_Shoot(object sender, EventArgs e)
        {
            var list = currentStage.GetElementsCollidableByBullet();
            var enemy = sender as Enemy;
            foreach (var en in list)
            {
                if (en != enemy)
                {
                    enemy.Bullet.CollidesWith(en);
                }

            }

            enemy.Bullet.CollidesWith(player1);

            if (!IsSingle)
            {
                enemy.Bullet.CollidesWith(player2);
            }
        }

        private void SpawnEnemy()
        {

            var blocking = new Boolean[currentStage.StageElements.GetLength(0),
                currentStage.StageElements.GetLength(1)];

            for (int i = 0; i < blocking.GetLength(0); i++)
            {
                for (int j = 0; j < blocking.GetLength(1); j++)
                {
                    if (currentStage.StageElements[i, j] == null ||
                        (currentStage.StageElements[i, j] != null &&
                        (currentStage.StageElements[i, j] is Icefield ||
                        currentStage.StageElements[i, j] is Bush)))
                    {
                        blocking[i, j] = false;
                    }
                    else
                    {
                        blocking[i, j] = true;
                    }
                }
            }

            for (int i = 0; i < enemies.Count; i++)
            {
                var enemy = enemies.ElementAt(i);

                if (!enemy.IsDestroyed())
                {
                    int x = enemy.CellPosition.X;
                    int y = enemy.CellPosition.Y;

                    blocking[x, y] = true;

                    if (enemy.NextCellPosition != null)
                    {
                        x = enemy.NextCellPosition.X;
                        y = enemy.NextCellPosition.Y;
                    }

                    blocking[x, y] = true;
                }
            }

            var cellList = player1.GetCellPositions();
            foreach (var c in cellList)
            {
                blocking[(int)c.X, (int)c.Y] = true;
            }

            if (!IsSingle)
            {
                cellList = player2.GetCellPositions();
                foreach (var c in cellList)
                {
                    blocking[(int)c.X, (int)c.Y] = true;
                }
            }

            List<Cell> firstHalfNonBlocking = new List<Cell>();
            List<Cell> secondHalfNonBlocking = new List<Cell>();

            for (int i = 0; i < blocking.GetLength(0); i++)
            {
                for (int j = 0; j < blocking.GetLength(1); j++)
                {
                    if (!blocking[i, j])
                    {
                        if (i < 6)
                        {
                            firstHalfNonBlocking.Add(new Cell(i, j));
                        }
                        else
                        {
                            secondHalfNonBlocking.Add(new Cell(i, j));
                        }
                    }
                }
            }

            Cell spawnPosition;

            if (firstHalfNonBlocking.Count > 0)
            {
                spawnPosition = firstHalfNonBlocking.ElementAt(r.Next(firstHalfNonBlocking.Count));
            }
            else
            {
                spawnPosition = secondHalfNonBlocking.ElementAt(r.Next(secondHalfNonBlocking.Count));
            }

            Enemy.EnemyType type = Enemy.EnemyType.EASY;

            if (numberOfEasyEnemies > 0)
            {
                type = Enemy.EnemyType.EASY;
                numberOfEasyEnemies--;
            }
            else if (numberOfMediumEnemies > 0)
            {
                type = Enemy.EnemyType.MEDIUM;
                numberOfMediumEnemies--;
            }
            else if (numberOfHardEnemies > 0)
            {
                type = Enemy.EnemyType.HARD;
                numberOfHardEnemies--;
            }


            var temp = new Enemy(spawnPosition, type);
            temp.Shoot += enemy_Shoot;
            temp.DestroyEvent += enemy_DestroyEvent;

            if(IsSingle || IsServer) 
            {
                player1.CollidesWith(temp);
            }
            if (IsServer)
            {
                var evnt = new NetworkEnemySpawnEvent();
                evnt.enemyType = type;
                evnt.cellPosition = spawnPosition;
                evnt.dropPowerUp = temp.DropPowerUp;
                evnt.powerUpType = temp.PowerUpType;
                server.SendEvent(evnt);
            }
        }

        public void enemy_DestroyEvent(object sender, EventArgs e)
        {
            var enemy = sender as Enemy;

            numberOfEnemiesSpawned--;
            enemyKilled++;

            if (enemyKilled == 20)
            {
                victory = true;
            }

            if (enemy.DropPowerUp)
            {
                var pu = new PowerUp(enemy.PowerUpType, enemy.Position, powerUps.Count);

                powerUps.Add(pu);

                if (IsServer || IsSingle)
                {
                    player1.CollidesWith(pu);
                }

                if (IsClient)
                {
                    player2.CollidesWith(pu);
                }
            }

        }

        public List<PowerUp> PowerUps
        {
            get { return powerUps; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
			if (currentStage != null && spriteBatch != null)
            {
                currentStage.Draw(spriteBatch);
            }
        }

        public void Update(GameTime gameTime)
        {

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {

                if (Game1.Instance.State == GameState.InSP)
                {
                    Game1.Instance.State = GameState.SPPause;
                    pause = true;
                }
                else if (Game1.Instance.State == GameState.InMP)
                {
                    Game1.Instance.State = GameState.MPPause;
                }
            }

            if (!pause)
            {
                if (currentStage != null)
                {
                    if (victory)
                    {
                        if (IsSingle) 
                        { 
                            Game1.Instance.State = GameState.SPLoading;
                        }
                        else
                        {
                            Game1.Instance.State = GameState.MPLoading;
                        }

                        stageIdx++;

                        if (stageIdx == StageDescriptions.NumberOfStages)
                        {
                            stageIdx = 0;

                            if (IsSingle)
                            {
                                Game1.Instance.State = GameState.SPCompleted;
                            }
                            else
                            {
                                Game1.Instance.State = GameState.MPCompleted;
                            }
                        }
                    }
                    else if (gameOver)
                    {
                        stageIdx = 0;

                        if (IsSingle)
                        {
                            Game1.Instance.State = GameState.SPGameOver;
                            stageIdx = 0;
                        }
                        else
                        {
                            Game1.Instance.State = GameState.MPGameOver;
                        }
                    }
                    else
                    {
                        if (!Settings.TESZT)
                        {
                            if ((player1.Lives <= 0 && IsSingle) || (!IsSingle && player1.Lives <= 0 && player2.Lives <= 0)
                            || currentStage.IsEagleDestroyed)
                            {
                                gameOver = true;
                            }
                        }

                        if (IsSingle || IsServer)
                        {
                            elapsedGameTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

                            if (elapsedGameTime >= enemySpawnTimeOut && numberOfEnemiesSpawned < stageIdx+1 && enemyKilled + numberOfEnemiesSpawned < 20)
                            {
                                SpawnEnemy();
                                numberOfEnemiesSpawned++;
                                elapsedGameTime = 0;
                            }

                            if (!IsSingle)
                            {
                                elapsedGameTimeForBroadCast += (float)gameTime.ElapsedGameTime.TotalSeconds;

                                if (elapsedGameTimeForBroadCast >= broadCastTimeOut) 
                                { 
                                    BroadCastEnemiesToClient();
                                    elapsedGameTimeForBroadCast = 0.0f;
                                }
                            }


                        }
                    }


                    currentStage.Update(gameTime);
                }
            }

        }

        public void UpdateStageEventToPartner(BasicSprite stageElement, int decreaseValue)
        {
            if (stageElement is BrickWall)
            {
                var bw = stageElement as BrickWall;
                Controller.Instance.SendNetworkEvent(new NetworkStageEvent() { cellPos = bw.CellPosition, decreaseValue = decreaseValue });
            }
            else if (stageElement is SteelWall)
            {
                var sw = stageElement as SteelWall;
                Controller.Instance.SendNetworkEvent(new NetworkStageEvent() { cellPos = sw.CellPosition, decreaseValue = decreaseValue });
            }
            else if (stageElement is Eagle)
            {
                var e = stageElement as Eagle;
                Controller.Instance.SendNetworkEvent(new NetworkStageEvent() { cellPos = e.CellPosition, decreaseValue = decreaseValue });
            }

        }

        public void UpdateStageEventFromPartner(NetworkStageEvent e)
        {
            var stageElement = currentStage.StageElements[e.cellPos.X, e.cellPos.Y];

            if (stageElement is BrickWall)
            {
                var bw = stageElement as BrickWall;
                bw.DecrementShotsCount(e.decreaseValue);
            }
            else if (stageElement is SteelWall)
            {
                var sw = stageElement as SteelWall;
                sw.DecrementShotsCount(e.decreaseValue);
            }
            else if (stageElement is Eagle)
            {
                var eag = stageElement as Eagle;
                eag.DecrementShotsCount(e.decreaseValue);
            }
        }

        private void BroadCastEnemiesToClient()
        {
            foreach (var enemy in enemies)
            {
                if (!enemy.IsDestroyed())
                {
                    var evnt = new NetworkEnemyMovingEvent();
                    evnt.direciton = enemy.Direction;
                    evnt.idx = enemy.EnemyIdx;
                    evnt.moving = enemy.IsMovingAnimationOn;
                    evnt.position = enemy.Position;
                    SendNetworkEvent(evnt);
                }
            }
        }

        public List<Enemy> Enemies 
        {
            get { return enemies; }
        }


        public bool ConnectedToServer 
        {
            get
            {
                return client.IsConnected();
            }
        }
    }
}
