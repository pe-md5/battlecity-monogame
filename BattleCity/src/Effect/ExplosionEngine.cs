﻿using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Utils;

namespace BattleCity.Effect
{
    public class ExplosionEngine
    {
        List<Explosion> explosions;

        private ExplosionEngine()
        {
            explosions = new List<Explosion>();
        }

        static ExplosionEngine instance;

        public static ExplosionEngine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExplosionEngine();
                }
                return instance;
            }
        }

        public void Add(Vector2 position) 
        {
            explosions.Add(new Explosion
                (new AnimatedSpriteImage(Resources.ExplosionTexture, 
                    Settings.NumberOfExplosionFrames, Settings.ExplosionAnimationSecPerFrame),
                    position, explosions));
        }

        public void Update(GameTime gameTime)
        {
            for(int i = 0; i < explosions.Count; i++)
            {
                var expl = explosions.ElementAt(i);
                expl.Update(gameTime);
                if (expl.ExplosionEnded)
                {
                    explosions.RemoveAt(i);
                    i++;
                }
                
            }
        }

        public void Draw(SpriteBatch batch)
        {
            foreach (var expl in explosions)
            {
                expl.Draw(batch);
            }
        }

    }
}
