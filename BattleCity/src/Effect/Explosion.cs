﻿using BattleCity.Sprite;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Effect
{
    public class Explosion : BasicSprite
    {
        List<Explosion> explosions;

        int idx;

        public Explosion(AnimatedSpriteImage simage, Vector2 position, List<Explosion> explosions) : base (simage, position)
        {
            this.explosions = explosions;
            idx = explosions.Count;
        }

        public override void Update(GameTime gameTime)
        {
            var image = (GetSpriteImage() as AnimatedSpriteImage);
            if (image != null)
            {
                if (image.FrameCount - 1 == image.CurrentFrame)
                {
                    ExplosionEnded = true;
                }
            }

            base.Update(gameTime);
        }

        public bool ExplosionEnded { get; private set; }
    }
}
